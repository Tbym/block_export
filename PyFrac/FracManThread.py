import time
import subprocess

class FracManThread(object):
    def __init__(self):
        self.fracManExePath = "C:\\FracMan_programming\\Trunk2\\Fred\\x64\\Release\\FracManx64.exe"
        self.nonRespondedTime = 0.0 #first time the process was Not Responding
        self.maxNonRespondedTime = 6000.0 #max time for Not Responding process
        self.timeOut = 50000.0 #Max total time for the process
        self.startTime = 0 #simulation start time

    def Run(self, macroFilePath, showWindow = False):
        """run FracMan with the macro"""
        systemCommand = "\"" + self.fracManExePath + "\" \"" + macroFilePath + "\""
        info = subprocess.STARTUPINFO()
        info.dwFlags = 1
        info.wShowWindow = showWindow

        #start the process
        p = subprocess.Popen(systemCommand, stdin=subprocess.PIPE, startupinfo=info)
        self.startTime = time.time()
        time.sleep(5) #wait 5 second before the first check
        #check the process
        while True:
            t = time.time()
            if t - self.startTime > self.timeOut:
                p.terminate()
                print("TIME_OUT: {0}".format(macroFilePath))
                return False
            if not self.IsResponding_PID(p.pid): #process is not responding or finished already
                if self.nonRespondedTime == 0: #first time the process was not responding
                    self.nonRespondedTime = t
                if t - self.nonRespondedTime > self.maxNonRespondedTime:
                    p.terminate()
                    print("NOT_RESPONDING: {0}".format(macroFilePath))
                    return False
            else: #sometime a process can be Not Responding for a short time and become active after
                self.nonRespondedTime = 0

            if p.poll() != None: #check if the process finished
                #print("NORMAL_FINISH: {0}".format(macroFilePath))
                break #process has finished

            time.sleep(10)

        return True

    def IsResponding_PID(self, pid):
        """Check if a program (based on its PID) is responding"""
        cmd = 'tasklist /FI "PID eq %d" /FI "STATUS eq running"' % pid
        status = subprocess.Popen(cmd, stdout=subprocess.PIPE).stdout.read()
        return str(pid) in str(status)