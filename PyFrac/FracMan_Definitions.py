import copy
from .FracObj.vec3 import *

class IntensityType():
    P32 = 0
    P33 = 1
    COUNT = 2

class DistributionType():
    CONSTANT = "CONSTANT"
    POWERLAW = "POWERLAW"
    EXPONENTIAL = "EXPONENTIAL"
    LOGNORMAL = "LOGNORMAL"

class CorrelationType():
    CONSTANT = "CONSTANT"
    EQUATION = "EQUATION"
    DISTANCE = "Distance"

class ValueType():
    Aperture = "Aperture"
    Transmissivity = "Transmissivity"
    Storativity = "Storativity"
    Pressure = "Pressure"
    Angle = "Angle"

class FilterType():
    WELL_LOGS = "Well Logs"
    FRACTURE_SETS = "Fracture Sets"
    POINT_DATA = "Point Data"
    TRACEMAPS = "Tracemaps"

class StressType():
    SYM_MATRIX = "Symmetric Matrix"
    PRINCIPAL_STRESS = "Principal Stresses"


class PropertyDef():
    def __init__(self):
        self.propName = ""
        self.valueType = ValueType()
        self.units = ""
        self.distributionType = DistributionType()
        self.correlationType = CorrelationType()
        self.element = 128
        self.function = ""
        self.minValue = 0.0
        self.param1 = 0.0
        self.param2 = 0.0
        self.param3 = 0.0
        self.toObject = ""
        self.toInstersection = 0
        self.vertOnly = 0
        self.horizOnly = 0
        self.selectionType = 0
        self.dataInterpolation = "MIN"

class FractureSetDef(object):
    def __init__(self, orig = None):

        if isinstance(orig, FractureSetDef):
            self.__dict__ = copy.deepcopy(orig.__dict__)
        else:
            self.name = ""
            self.regionName = ""
            self.clipToRegion = True
            self.intensityType = IntensityType()
            self.intensityValue = 0.0

            self.trend = 0.0
            self.plunge = 0.0
            self.fisherK = 0.0
            self.powerLaw_Xmin = 0.0
            self.powerLaw_D = 0.0
            self.powerLaw_minVal = 0.0
            self.powerLaw_maxVal = 0.0
            self.num_sides = 0
            self.properties = []

            prop = PropertyDef()
            prop.propName = "Aperture"
            prop.valueType = ValueType.Aperture
            prop.distributionType = DistributionType.CONSTANT
            prop.param1 = 1e-006
            prop.units = "m"
            self.properties.append(prop)

            prop = PropertyDef()
            prop.propName = "Transmissivity"
            prop.valueType = ValueType.Transmissivity
            prop.distributionType = DistributionType.CONSTANT
            prop.param1 = 1e-006
            prop.units = "m2/s"
            self.properties.append(prop)

            prop = PropertyDef()
            prop.propName = "Storativity"
            prop.valueType = ValueType.Storativity
            prop.distributionType = DistributionType.CONSTANT
            prop.param1 = 1e-006
            prop.units = "-"
            self.properties.append(prop)

class FractureGeoCellularDef(FractureSetDef):
    def __init__(self, orig = None):
        super(FractureGeoCellularDef, self).__init__(orig)
        if isinstance(orig, FractureGeoCellularDef):
            self.__dict__ = copy.deepcopy(orig.__dict__)
        else:
            self.gridName = ""
            self.intensityCorrName = ""
            self.intensityCorrConst = []
            self.storeGeocellPotential = 0
            self.trendCorrName = ""
            self.trendCorrConst = []
            self.plungeCorrName = ""
            self.plungeCorrConst = []
            self.orientationCorrName = ""
            self.orientationCorrConst = []
            self.elongDist = DistributionType.CONSTANT
            self.elongParam = 1


class FractureGrowthDef(object):
    def __init__(self):
        self.name = ""
        self.gridName = ""
        self.subsets = []



class FractureGrowthSubSetDef(FractureSetDef):
    def __init__(self, orig = None):
        super(FractureGrowthSubSetDef, self).__init__(orig)
        if isinstance(orig, FractureGrowthSubSetDef):
            self.__dict__ = copy.deepcopy(orig.__dict__)
        else:
            self.setImpedance = 1
            self.generationTimesteps = 100
            self.growthTimesteps = 10
            self.subsetTermination = []
            self.maxArrayArrest = 50
            self.shadowZoneFactor = 0.1
            self.maxSeedFailure = 95
            self.zoneFactor = 0.1
            self.hookZoneFactor = 0
            self.relativeIntensity = ""



class SurfaceIntersectionDef(object):
    def __init__(self):
        self.name = ""
        self.objectNames = []
        self.fracSetNames = []
        self.computeTraces = True
        self.exportTracemap = False
        self.tracemapPath = ""
        self.newObjectName = ""
        self.computeFracs = False
        self.computeConnected = False
        self.computeStats = False

    def AddFractureSets(self, sets, suffix= ""):

        for s in sets:
            name = s.name + suffix
            self.fracSetNames.append(name)

class SubsetDef(object):
    def __init__(self):
        self.objectNames = []
        self.subsetName = ""
        self.createEmptySubsets = False
        self.categoryProperty = ""
        self.catStrValue = ""
        self.catMinVal = 0.0
        self.catMaxVal = 0.0

class FilterDef(object):
    def __init__(self):
        self.objectNames = []
        self.name = ""
        self.elementType = "Lines" # "Faces"
        self.filterType = FilterType()
        self.propName = []
        self.propMin = []
        self.propMax = []
        self.propBool = []


class RockWedgeDef(object):
    def __init__(self):
        self.name = ""
        self.sampleName = ""
        self.fracSetNames = []
        self.inputFilter = ""
        self.MaxConnectionLevel = 6
        self.IsTunnelStructure = 0

        self.RockDensity = 0.0
        self.SeismicAcceleration = 0
        self.PhreaticSurfaceName = ""    #to use a water pressure surface
        self.WaterPressure = 0.0    #to use a constant water pressure
        self.UseStress = 0
        self.Stress_Grid = ""

        self.Stress_Type = StressType()
        self.Stress_S1 = "BasicGrid_1->FracStress_S1"
        self.Stress_S2 = "BasicGrid_1->FracStress_S2"
        self.Stress_S3 = "BasicGrid_1->FracStress_S3"
        self.Stress_T1 = "BasicGrid_1->FracStress_S1_Trend"
        self.Stress_P1 = "BasicGrid_1->FracStress_S1_Plunge"
        self.Stress_T3 = "BasicGrid_1->FracStress_S3_Trend"
        self.Stress_P3 = "BasicGrid_1->FracStress_S3_Plunge"

        self.Stress_XX = "BasicGrid_1->FracStress_XX"
        self.Stress_YY = "BasicGrid_1->FracStress_YY"
        self.Stress_ZZ = "BasicGrid_1->FracStress_ZZ"
        self.Stress_XY = "BasicGrid_1->FracStress_XY"
        self.Stress_XZ = "BasicGrid_1->FracStress_XZ"
        self.Stress_YZ = "BasicGrid_1->FracStress_YZ"



        self.IsBartonModel = 0
        self.PorePressure = 0.0
        self.FrictionAngle = 0.0
        self.Cohesion = 0.0

        self.PorePressureProp = ""
        self.FrictionAngleProp = ""
        self.CohesionProp = ""

        self.UseAllBoltPanels = 1
        self.UseDipBoltPanel = 0
        self.UseZBoltPanel = 0
        self.MinDipBoltPanel = 0
        self.MinZBoltPanel = 0
        self.CombineBlock = 0
        self.SaveBoth = 0
        self.DisplayFactorOfSafety = 0
        self.DisplayStats = 1
        self.DisplayVolume = 0
        self.DisplayWeight = 0
        self.OutputDirectoryName = ""
        self.AdjustFOSUsingCompositeBlocks = 1
        self.SaveCompositeBlocks = 1
        self.OutputForceDueToStress = 0
        self.OutputCriticalStressStats = 0


class SlopeDef(object):
    def __init__(self):
        self.name = ""
        self.origin = vec3()
        self.crestDirTr = 0.0
        self.crestDirPl = 0.0
        self.crestLength = 0.0
        self.crestWidth = 0.0
        self.toeLength = 0.0
        self.toeWidth = 0.0
        self.benchAngle = 0.0
        self.benchWidth = 0.0
        self.benchCount = 0.0
        self.benchHeight = 0.0
        self.rampWidth = 0.0
