import os
import copy
from .FracMan_Definitions import *



class FracManMacro(object):
    def __init__(self):
        self.__templateDir = os.path.join(os.path.dirname(os.path.realpath(__file__)),"Macro_templates")
        self.__macroData = ""

    def WriteMacro(self, outputFile):
        """
            Write macro to the output file
            :return:
        """
        with open(outputFile,'w+') as f:
            f.write(self.__macroData)

    def AddToMacro(self, macroData):
        """
        Append macro text to current macro buffer
        :param macroData: input text
        :return:
        """
        self.__macroData += macroData

    def CloseFracMan(self, addToBuffer = True):
        macroData = "EXIT\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def SaveFracFile(self, outputFile, addToBuffer = True):

        macroData = "BEGIN ExportFile\n"

        macroData += "\tPath = \"" + outputFile + "\"\n"

        macroData += "\tTypeName = \"FRED\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def LoadFracFile(self, inputFile, addToBuffer = True):

        macroData = "BEGIN ImportFile\n"

        macroData += "\tPath = \"" + inputFile + "\"\n"

        macroData += "\tTypeName = \"FRED\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def ImportObject(self, inputFile, objType, addToBuffer = True):

        macroData = "BEGIN ImportFile\n"

        macroData += "\tPath = \"" + inputFile + "\"\n"

        macroData += "\tTypeName = \"" + objType + "\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def ExitFrac(self, addToBuffer = True):

        macroData = "EXIT\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def __AddFracSetProp(self, propDef):

        macroData = "\t\tName = \"" + propDef.propName + "\"\n"
        macroData += "\t\tBEGIN Distribution\n"
        macroData += "\t\t\tValueType \"" + str(propDef.valueType) + "\" Units \"" + str(propDef.units) + "\"\n"
        macroData += "\t\t\tType = \"" + str(propDef.distributionType) + "\"\n"

        if propDef.distributionType == DistributionType.CONSTANT:
            macroData += "\t\t\tParameter \"Value\" = {0}\n".format(propDef.param1)
        elif propDef.distributionType == DistributionType.POWERLAW:
            macroData += "\t\t\tParameter \"Xmin\" = {0}\n".format(propDef.param1)
            macroData += "\t\t\tParameter \"D\" = {0}\n".format(propDef.param2)
        else:
            print ("Distribution type not supported")

        macroData += "\t\t\tMinValue = {0}\n".format(propDef.minValue)

        macroData += "\t\tEND\n"

        return macroData

    def __AddFractureGrowthSubset(self, fractureDef):

        macroData = "\tBEGIN Subset\n"
        macroData += "\t\tSubsetName = \"" + fractureDef.name + "\"\n"
        macroData += "\t\tSetImpedance = {0}\n".format(fractureDef.setImpedance)
        macroData += "\t\tGenerationTimesteps = {0}\n".format(fractureDef.generationTimesteps)
        macroData += "\t\tGrowthTimesteps = {0}\n".format(fractureDef.growthTimesteps)
        for v in fractureDef.subsetTermination:
            macroData += "\t\tSubsetTermination = {0}\n".format(v)
        macroData += "\t\tMaxRayArrest = {0}\n".format(fractureDef.maxArrayArrest)
        macroData += "\t\tShadowZoneFactor = {0}\n".format(fractureDef.shadowZoneFactor)
        macroData += "\t\tHookZoneFactor = {0}\n".format(fractureDef.hookZoneFactor)
        macroData += "\t\tMaxSeedFailure = {0}\n".format(fractureDef.maxSeedFailure)

        if fractureDef.intensityType == IntensityType.P32:
            macroData += "\t\tP32 = {0}\n".format(fractureDef.intensityValue)
        elif fractureDef.intensityType == IntensityType.P33:
            macroData += "\t\tP33 = {0}\n".format(fractureDef.intensityValue)
        elif fractureDef.intensityType == IntensityType.COUNT:
            macroData += "\t\tcount = {0}\n".format(fractureDef.intensityValue)
        macroData += "\t\tRelativeIntensity = {0}\n".format(fractureDef.relativeIntensity)

        # orientation
        macroData += "\t\tUsePole = 1\n"
        macroData += "\t\tBEGIN OrientationDistribution\n"
        macroData += "\t\t\tType = \"FISHER\"\n"
        macroData += "\t\t\tParameter \"Mean Trend\" = {0}\n".format(fractureDef.trend)
        macroData += "\t\t\tParameter \"Mean Plunge\" = {0}\n".format(fractureDef.plunge)
        macroData += "\t\t\tParameter \"Concentration\" = {0}\n".format(fractureDef.fisherK)
        macroData += "\t\tEND\n"

        # size
        macroData += "\t\tBEGIN SizeDistribution\n"
        macroData += "\t\t\tValueType \"Length\" Units \"m\"\n"
        macroData += "\t\t\tType = \"POWERLAW\"\n"
        macroData += "\t\t\tParameter \"Xmin\" = {0}\n".format(fractureDef.powerLaw_Xmin)
        macroData += "\t\t\tParameter \"D\" = {0}\n".format(fractureDef.powerLaw_D)
        if fractureDef.powerLaw_minVal > 0:
            macroData += "\t\t\tMinValue = {0} [m]\n".format(fractureDef.powerLaw_minVal)
        if fractureDef.powerLaw_maxVal > 0:
            macroData += "\t\t\tMaxValue = {0} [m]\n".format(fractureDef.powerLaw_maxVal)
        macroData += "\t\tEND\n"

        macroData += "\t\tZoneFactor = {0}\n".format(fractureDef.zoneFactor)
        macroData += "\t\tHookZoneFactor = {0}\n".format(fractureDef.hookZoneFactor)

        macroData += "\t\tNumSides = {0:d}\n".format(fractureDef.num_sides)

        macroData += "\t\tBEGIN Properties\n"
        for prop in fractureDef.properties:
            macroData += self.__AddFracSetProp(prop)
        macroData += "\t\tEND\n"

        macroData += "\tEND\n"

        return macroData

    def AddFractureGrowthDef(self, fractureSetsDef, addToBuffer = True):

        macroData = "\tBEGIN DefineFractureGrowthSet\n"
        macroData += "\tName = \"" + fractureSetsDef.name + "\"\n"
        macroData += "\tGridName = \"" + fractureSetsDef.gridName + "\"\n"

        for s in fractureSetsDef.subsets:
            macroData += self.__AddFractureGrowthSubset(s)

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def AddProperty(self, propDef, objects, addToBuffer = True):
        macroData = "BEGIN AddProperty\n"

        #propDef = PropertyDef()

        for name in objects:
            if isinstance(name, str):
                macroData += "\tObject = \"" + name + "\"\n"

        macroData += "\tPropertyName = \"" + propDef.propName + "\"\n"
        macroData += "\tCorrelation = \"" + propDef.correlationType + "\"\n"
        if propDef.correlationType == CorrelationType.DISTANCE:
            macroData += "\tToObject = {0}\n".format(propDef.toObject)
            macroData += "\tToIntersection = {0}\n".format(propDef.toInstersection)
            macroData += "\tVertOnly = {0}\n".format(propDef.vertOnly)
            macroData += "\tHorizOnly = {0}\n".format(propDef.horizOnly)
            macroData += "\tSelectionType = {0}\n".format(propDef.selectionType)
            macroData += "\tDataInterpolation = {0}\n".format(propDef.dataInterpolation)
            macroData += "\tType = \"" + "single precision floating point" + "\"\n"

        else:
            if propDef.correlationType == CorrelationType.CONSTANT:
                macroData += "\tParaConst = {0}\n".format(propDef.param1)
            elif propDef.correlationType == CorrelationType.EQUATION:
                macroData += "\tFunction = \"" + propDef.function + "\"\n"
                macroData += "\tUseMean = {0}\n".format(0)
                macroData += "\tStatic = {0}\n".format(1)

            macroData += "\tNullValue = {0}\n".format(-9999)
            macroData += "\tElement = {0}\n".format(propDef.element)

            macroData += "\tType = \"" + "double precision floating point" + "\"\n"
            macroData += "\tValueType = \"" + propDef.valueType + "\"\n"
            macroData += "\tUnitsType = \"" + propDef.units + "\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData



    def AddFractureSet(self, fractureDef, addToBuffer = True):
        """
        Create fracture set definition macro string
        :param fractureDef: Fracture set definition
        :return: macroData string
        """
        macroData = "BEGIN DefineFractureSetDefinition\n"
        macroData += "\tDefinitionName = \"" + fractureDef.name + "\"\n"
        macroData += "\tFractureModel = \"REGION\"\n"
        macroData += "\tRegionName = \"" + fractureDef.regionName + "\"\n"
        macroData += "\tClipToRegion = {0:d}\n".format(fractureDef.clipToRegion)
        macroData += "\tUseSurfacePoints = 1\n"
        macroData += "\tBEGIN Properties\n"
        for prop in fractureDef.properties:
            macroData += self.__AddFracSetProp(prop)
        macroData += "\tEND\n"

        # write intensity
        if fractureDef.intensityType == IntensityType.P32:
            macroData += "\tP32 = {0}\n".format(fractureDef.intensityValue)
        elif fractureDef.intensityType == IntensityType.P33:
            macroData += "\tP33 = {0}\n".format(fractureDef.intensityValue)
        elif fractureDef.intensityType == IntensityType.COUNT:
            macroData += "\tcount = {0}\n".format(fractureDef.intensityValue)
        macroData += "\tAdjustedByTruncation = 0\n"

        #orientation
        macroData += "\tUsePole = 1\n"
        macroData += "\tBEGIN OrientationDistribution\n"
        macroData += "\t\tType = \"FISHER\"\n"
        macroData += "\t\tParameter \"Mean Trend\" = {0}\n".format(fractureDef.trend)
        macroData += "\t\tParameter \"Mean Plunge\" = {0}\n".format(fractureDef.plunge)
        macroData += "\t\tParameter \"Concentration\" = {0}\n".format(fractureDef.fisherK)
        macroData += "\tEND\n"

        #size
        macroData += "\tBEGIN SizeDistribution\n"
        macroData += "\t\tValueType \"Length\" Units \"m\"\n"
        macroData += "\t\tType = \"POWERLAW\"\n"
        macroData += "\t\tParameter \"Xmin\" = {0}\n".format(fractureDef.powerLaw_Xmin)
        macroData += "\t\tParameter \"D\" = {0}\n".format(fractureDef.powerLaw_D)
        if fractureDef.powerLaw_minVal > 0:
            macroData += "\t\tMinValue = {0} [m]\n".format(fractureDef.powerLaw_minVal)
        if fractureDef.powerLaw_maxVal > 0:
            macroData += "\t\tMaxValue = {0} [m]\n".format(fractureDef.powerLaw_maxVal)
        macroData += "\tEND\n"

        macroData += "\tNumSides = {0:d}\n".format(fractureDef.num_sides)
        macroData += "\tTermination = 0\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData


    def GenerateFractureSets(self, objsList, addToBuffer = True):
        """
        Generate fracture sets macro
        :param namesList: list of set names or FractureSetDef objects
        :return:
        """
        macroData = ""

        for obj in objsList:

            if isinstance(obj,FractureSetDef):
                macroData += "BEGIN GenerateFractureSet\n"
                macroData += "\tDefinitionName = \"" + obj.name + "\"\n"
            elif isinstance(obj, str):
                macroData += "BEGIN GenerateFractureSet\n"
                macroData += "\tDefinitionName = \"" + obj + "\"\n"
            elif isinstance(obj, FractureGrowthDef):
                macroData += "BEGIN GenerateFractureGrowthSet\n"
                macroData += "\tNAME = \"" + obj.name + "\"\n"


            macroData += "\t #FractureSetName = \"SetName\"  #Optional name for generated set - (replaces existing objects!)\n"

            macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def RockWedge(self, rockWedgeDef, bRunDef = True, addToBuffer=True):

        macroData = "BEGIN RockWedgeDefinition\n"

        macroData += "\tName = \"" + rockWedgeDef.name + "\"\n"
        if len(rockWedgeDef.inputFilter):
            macroData += "\tInputFilter = \"" + rockWedgeDef.inputFilter + "\"\n"
        else:
            for name in rockWedgeDef.fracSetNames:
                if isinstance(name, str):
                    macroData += "\tObject = \"" + name + "\"\n"

        macroData += "\tSampleName = \"" + rockWedgeDef.sampleName + "\"\n"
        macroData += "\tMaxConnectionLevel = {0}\n".format(rockWedgeDef.MaxConnectionLevel)
        macroData += "\tIsTunnelStructure = {0}\n".format(rockWedgeDef.IsTunnelStructure)
        macroData += "\tRockDensity = {0} [kg/m3]\n".format(rockWedgeDef.RockDensity)
        macroData += "\tSeismicAcceleration = {0}\n".format(rockWedgeDef.SeismicAcceleration)
        if len(rockWedgeDef.PhreaticSurfaceName):
            macroData += "\tPhreaticSurfaceName = \"" + rockWedgeDef.sampleName + "\"\n"
        if rockWedgeDef.WaterPressure > 0:
            macroData += "\tWaterPressure = {0}\n".format(rockWedgeDef.WaterPressure)

        macroData += "\tUseStress = {0}\n".format(rockWedgeDef.UseStress)

        if rockWedgeDef.UseStress == 1:
            macroData += "\tStressGrid = \"{0}\"\n".format(rockWedgeDef.Stress_Grid)
            macroData += "\tBEGIN StressTensor\n"
            macroData += "\t\tType = \"{0}\"\n".format(rockWedgeDef.Stress_Type)
            if rockWedgeDef.Stress_Type == StressType().PRINCIPAL_STRESS:

                macroData += "\t\tS1 = \"{0}\"\n".format(rockWedgeDef.Stress_S1)
                macroData += "\t\tS2 = \"{0}\"\n".format(rockWedgeDef.Stress_S2)
                macroData += "\t\tS3 = \"{0}\"\n".format(rockWedgeDef.Stress_S3)
                macroData += "\t\tT1 = \"{0}\"\n".format(rockWedgeDef.Stress_T1)
                macroData += "\t\tP1 = \"{0}\"\n".format(rockWedgeDef.Stress_P1)
                macroData += "\t\tT3 = \"{0}\"\n".format(rockWedgeDef.Stress_T3)
                macroData += "\t\tP3 = \"{0}\"\n".format(rockWedgeDef.Stress_P3)
            else:
                macroData += "\t\tXX = \"{0}\"\n".format(rockWedgeDef.Stress_XX)
                macroData += "\t\tYY = \"{0}\"\n".format(rockWedgeDef.Stress_YY)
                macroData += "\t\tZZ = \"{0}\"\n".format(rockWedgeDef.Stress_ZZ)
                macroData += "\t\tXY = \"{0}\"\n".format(rockWedgeDef.Stress_XY)
                macroData += "\t\tXZ = \"{0}\"\n".format(rockWedgeDef.Stress_XZ)
                macroData += "\t\tYZ = \"{0}\"\n".format(rockWedgeDef.Stress_YZ)

            macroData += "\tEND\n"

        macroData += "\tIsBartonModel = {0}\n".format(rockWedgeDef.IsBartonModel)
        if len(rockWedgeDef.PorePressureProp):
            macroData += "\tPorePressureProp = \"" + rockWedgeDef.PorePressureProp + "\"\n"
        else:
            macroData += "\tPorePressure = {0}  [Pa]\n".format(rockWedgeDef.PorePressure)
        if len(rockWedgeDef.FrictionAngleProp):
            macroData += "\tFrictionAngleProp = \"" + rockWedgeDef.FrictionAngleProp + "\"\n"
        else:
            macroData += "\tFrictionAngle = {0} [deg]\n".format(rockWedgeDef.FrictionAngle)
        if len(rockWedgeDef.CohesionProp):
            macroData += "\tCohesionProp = \"" + rockWedgeDef.CohesionProp + "\"\n"
        else:
            macroData += "\tCohesion = {0}\n".format(rockWedgeDef.Cohesion)

        macroData += "\tUseAllBoltPanels = {0}\n".format(rockWedgeDef.UseAllBoltPanels)
        macroData += "\tUseDipBoltPanel = {0}\n".format(rockWedgeDef.UseDipBoltPanel)
        macroData += "\tUseZBoltPanel = {0}\n".format(rockWedgeDef.UseZBoltPanel)
        macroData += "\tMinDipBoltPanel = {0} [deg]\n".format(rockWedgeDef.MinDipBoltPanel)
        macroData += "\tMinZBoltPanel = {0} [m]\n".format(rockWedgeDef.MinZBoltPanel)

        macroData += "\t#Output options\n"

        macroData += "\tCombineBlock = {0}\n".format(rockWedgeDef.CombineBlock)
        macroData += "\tSaveBoth = {0}\n".format(rockWedgeDef.SaveBoth)
        macroData += "\tDisplayFactorOfSafety = {0}\n".format(rockWedgeDef.DisplayFactorOfSafety)
        macroData += "\tDisplayStats = {0}\n".format(rockWedgeDef.DisplayStats)
        macroData += "\tDisplayVolume = {0}\n".format(rockWedgeDef.DisplayVolume)
        macroData += "\tDisplayWeight = {0}\n".format(rockWedgeDef.DisplayWeight)
        macroData += "\tOutputDirectoryName = \"{0}\"\n".format(rockWedgeDef.OutputDirectoryName)
        macroData += "\tAdjustFOSUsingCompositeBlocks = {0}\n".format(rockWedgeDef.AdjustFOSUsingCompositeBlocks)
        macroData += "\tSaveCompositeBlocks = {0}\n".format(rockWedgeDef.SaveCompositeBlocks)
        macroData += "\tOutputForceDueToStress = {0}\n".format(rockWedgeDef.OutputForceDueToStress)
        macroData += "\tOutputCriticalStressStats = {0}\n".format(rockWedgeDef.OutputCriticalStressStats)

        macroData += "END\n\n"

        if bRunDef:
            macroData += self.RunRockWedgeDef(rockWedgeDef.name, False)

        if addToBuffer:
            self.__macroData += macroData

        return macroData



    def RunRockWedgeDef(self, defName, addToBuffer = True):
        macroData = "BEGIN GenerateRockWedge\n"
        macroData += "\tDefinitionName = \"" + defName + "\"\n"
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def RunFoSUpdateRockWedgeDef(self, defName, addToBuffer = True):
        macroData = "BEGIN DoStabilityAnalysis\n"
        macroData += "\tDefinitionName = \"" + defName + "\"\n"
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def DeleteObjects(self, objNames, addToBuffer = True):

        macroData = "BEGIN DeleteObjects\n"
        for name in objNames:
            if isinstance(name, str):
                macroData += "\tName = \"" + name + "\"\n"
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def SetSeed(self, seed, addToBuffer = True):

        macroData = "BEGIN SetSeed\n"
        macroData += "\tSeed = {0:d}\n".format(seed)
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def ExportObject(self, objName, objType, path, addToBuffer = True):

        macroData = "BEGIN ExportFile\n"

        macroData += "\tPath = \"" + path + "\"\n"
        macroData += "\tTypeName = \"" + objType + "\"\n"
        macroData += "\tObject = \"" + objName + "\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData


    def ObjectStats(self, namesList, outputFile = "", addToBuffer = True):
        """
        Compute objects stats and export to output file in .sts format
        :param namesList: list of object names
        :param outputFile: .sts output file
        :param addToBuffer:
        :return:
        """
        macroData = "BEGIN ObjectStats\n"
        for name in namesList:
            if isinstance(name, str):
                macroData += "\tObject = \"" + name + "\"\n"

        macroData += "\tView = 0\n"

        if len(outputFile):
            macroData += "\tPath = \"" + outputFile + "\"\n"

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def PowerLawPlot(self, namesList, propertyName, outputFile = "", addToBuffer = True):
        """
        :param namesList:
        :param outputFile:
        :param addToBuffer:
        :return:
        """
        macroData = "BEGIN PowerLawPlot\n"

        if len(outputFile):
            macroData += "\tPath = \"" + outputFile + "\"\n"

        for name in namesList:
            if isinstance(name, str):
                macroData += "\tObject = \"" + name + "\"\n"

        macroData += "\tProperty = \"" + propertyName + "\"\n"



        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def SurfaceIntersection(self, surfItscDef, bRunDef = True, addToBuffer = True):

        macroData = "BEGIN DefineSurfaceIntersect\n"
        macroData += "\tDefName = \"" + surfItscDef.name + "\"\n"
        for name in surfItscDef.objectNames:
            if isinstance(name, str):
                macroData += "\tObjectName = \"" + name + "\"\n"

        for name in surfItscDef.fracSetNames:
            if isinstance(name, str):
                macroData += "\tFractureSet = \"" + name + "\"\n"

        macroData += "\tComputeTraces = {0:d}\n".format(surfItscDef.computeTraces)
        macroData += "\tExportTracemap = {0:d}\n".format(surfItscDef.exportTracemap)
        macroData += "\tTracemapPath = \"" + surfItscDef.tracemapPath + "\"\n"
        macroData += "\tComputeFracs = {0:d}\n".format(surfItscDef.computeFracs)
        macroData += "\tComputeConnected = {0:d}\n".format(surfItscDef.computeConnected)
        macroData += "\tComputeStats = {0:d}\n".format(surfItscDef.computeStats)

        macroData += "END\n\n"

        if bRunDef:
            macroData += self.RunSurfaceIntersection(surfItscDef.name, False)

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def InsertSlope(self, slopeDef, addToBuffer = True):

        macroData = "BEGIN CreateSlope\n"

        macroData += "\tObjectName = \"" + slopeDef.name + "\"\n"
        macroData += "\tOrigin = {0}, {1}, {2}\n".format(slopeDef.origin[0], slopeDef.origin[1], slopeDef.origin[2])
        macroData += "\tCrestDirection trend = {0} plunge = {1}\n".format(slopeDef.crestDirTr, slopeDef.crestDirPl)
        macroData += "\tCrestLength = {0}\n".format(slopeDef.crestLength)
        macroData += "\tCrestWidth = {0}\n".format(slopeDef.crestWidth)
        macroData += "\tToeLength = {0}\n".format(slopeDef.toeLength)
        macroData += "\tToeWidth = {0}\n".format(slopeDef.toeWidth)
        macroData += "\tBenchAngle = {0}\n".format(slopeDef.benchAngle)
        macroData += "\tBenchWidth = {0}\n".format(slopeDef.benchWidth)
        macroData += "\tBenchCount = {0:d}\n".format(slopeDef.benchCount)
        macroData += "\tBenchHeight = {0}\n".format(slopeDef.benchHeight)
        macroData += "\tRampWidth = {0}\n".format(slopeDef.rampWidth)

        macroData += "END\n\n"


        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def InsertRegionBox(self, name, origin, boxSize, addToBuffer = True):
        macroData = "BEGIN CREATEREGION\n"
        macroData += "\tObjectName = \"" + name + "\"\n"
        macroData += "\tType = \"Box\"\n"
        macroData += "\tCenter = {0}, {1}, {2}\n".format(origin[0], origin[1], origin[2])
        macroData += "\tSize = {0}, {1}, {2}\n".format(boxSize[0], boxSize[1], boxSize[2])
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def SetLengthUnits(self, unit, addToBuffer = True):

        macroData = "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"Length\"\n"
        macroData += "\tUnits = \"" + unit + "\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"Depth\"\n"
        macroData += "\tUnits = \"" + unit + "\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"Area\"\n"
        macroData += "\tUnits = \"" + unit + "2\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"Volume\"\n"
        macroData += "\tUnits = \"" + unit + "3\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"Aperture\"\n"
        macroData += "\tUnits = \"" + unit + "\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"P10\"\n"
        macroData += "\tUnits = \"1/" + unit + "\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"P21\"\n"
        macroData += "\tUnits = \"1/" + unit + "\"\n"
        macroData += "END\n\n"

        macroData += "BEGIN SetCurrentUnits\n"
        macroData += "\tValue = \"P32\"\n"
        macroData += "\tUnits = \"1/" + unit + "\"\n"
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def RunSurfaceIntersection(self, defName, addToBuffer = True):
        macroData = "BEGIN RunSurfaceIntersect\n"
        macroData += "\tDefName = \"" + defName + "\"\n"
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def CreateSubset(self, subsetDef, addToBuffer = True):

        macroData = ""
        if len(subsetDef.catStrValue):
            macroData = "BEGIN CreateCategorySubsets\n"
        else:
            macroData = "BEGIN CreateSubsetsFromRanges\n"

        for name in subsetDef.objectNames:
            if isinstance(name, str):
                macroData += "\tObjectName = \"" + name + "\"\n"

        macroData += "\tSubsetName = \"" +subsetDef.subsetName + "\"\n"
        macroData += "\tCreateEmptySubsets = {0:d}\n".format(subsetDef.createEmptySubsets)
        macroData += "\tCategoryProperty = \"" + subsetDef.categoryProperty + "\"\n"
        if len(subsetDef.catStrValue):
            macroData += "\tCategoryValue = \"" + subsetDef.catStrValue + "\"\n"
        else:
            macroData += "\tCategoryValue = \"{0}-{1}\"\n".format(subsetDef.catMinVal, subsetDef.catMaxVal)

        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def ClipFractures(self, surfName, fracSetName, clipPositive=0, addToBuffer=True):
        macroData = "BEGIN CLIPFRACTURES\n"
        macroData += "\tObject = \"" + surfName + "\"\n"
        macroData += "\tClipToPositive = {0:d}\n".format(clipPositive)
        for s in fracSetName:
            macroData += "\tFractureSet = {0}\n".format(s)
        macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

    def AddFilter(self, filterDef, bRunDef = True, addToBuffer=True):


        macroData = "BEGIN DefineFilter\n"
        macroData += "\tName = \"" + filterDef.name + "\"\n"
        macroData += "\tElementType = \"" + filterDef.elementType + "\"\n"
        macroData += "\tFilterType = \"" + filterDef.filterType + "\"\n"
        for i in range(len(filterDef.propName)):
            macroData += "\tProperty Name = \"{0}\" MinValue = \"{1}\" MaxValue = \"{2}\" Bool = \"{3}\"\n".format(filterDef.propName[i], filterDef.propMin[i], filterDef.propMax[i], filterDef.propBool[i])

        for o in filterDef.objectNames:
            macroData += "\tObject = \"" + o + "\"\n"

        macroData += "END\n\n"

        if bRunDef:
            macroData += "BEGIN CreateTracemapSubset\n"
            macroData += "\tFilter = \"" + filterDef.name + "\"\n"
            macroData += "END\n\n"

        if addToBuffer:
            self.__macroData += macroData

        return macroData

