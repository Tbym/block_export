from G3dCore import *
from GArrows import *
from vec3 import *

class CBootstrapData(G3dCore):
    def __init__(self):
        self.m_type_i32 = 0
        self.m_sizeProp_str = ""
        self.m_normalizeSize_b = True
        self._m_trendProp_strV = ["", ""]
        self._m_plungeProp_strV = ["", ""]
        self._m_rakeProp_strV = ["", ""]
        self.m_arrows = GArrows()
        self.m_points = []
        self.m_props = GProperties()
        super(CBootstrapData, self).__init__()

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            percentDisp = ar.ReadDbl64()

            if ar.GetObjectVersion() >= 3:
                visibleProp = ar.ReadStr()

            if ar.GetObjectVersion() >= 4:
                self.m_sizeProp_str = ar.ReadStr()
                self.m_normalizeSize_b = ar.ReadUInt8()
                self._m_trendProp_strV[0] = ar.ReadStr()
                self._m_plungeProp_strV[0] = ar.ReadStr()

            if ar.GetObjectVersion() >= 5:
                self.m_arrows.SerializedContained(ar)

            if ar.GetObjectVersion() >= 6:
                self._m_rakeProp_strV[0] = ar.ReadStr()

            if ar.GetObjectVersion() >= 7:
                self._m_trendProp_strV[1] = ar.ReadStr()
                self._m_plungeProp_strV[1] = ar.ReadStr()
                self._m_rakeProp_strV[1] = ar.ReadStr()

    def SerializeAttributes(self, ar):

        super(CBootstrapData, self).SerializeAttributes(ar)

        if ar.GetArchiveVersion() >= 751:
            self.m_props.Serialize(ar)


        if ar.m_isReading:
            count = ar.ReadInt32()

            for i in range(count):
                v = vec3()
                v[0] = ar.ReadDbl64()
                v[1] = ar.ReadDbl64()
                v[2] = ar.ReadDbl64()
                self.m_points.append(v)

