from CParameter import *
from CVector import *

class CDistribution(object):
    def __init__(self):
        self.m_Type_i32 = 0
        self.m_bMinTruncated_b = False
        self.m_bMaxTruncated_b = False
        self.m_minValue = CParameter()
        self.m_maxValue = CParameter()
        self.m_ParametersNames = []
        self.m_Parameters = []
        self.m_bCorrelated_b = False
        self.m_CorrelatedToName_str = ""
        self.m_CorrelationConstants = []
        self.m_bootStrapInfo = []
        self.m_bootStrapSubDist_i32 = 0
        self.m_multiBootstrap_b = False
        self.m_bootstrapRadius = 0.0

    def Serialize(self, ar):
        if ar.m_isReading:
            bReadCorr = False
            nType = ar.ReadInt32()

            if nType < 0:
                bReadCorr = True
                nType =  - nType - 1

            self.m_Type_i32 = nType

            legacyVal = ar.ReadInt32()
            bTruncated = ar.ReadInt32()

            if bTruncated == -1:
                bVal = ar.ReadInt32()
                self.m_bMinTruncated_b = (0 != bVal)

                bVal = ar.ReadInt32()
                self.m_bMaxTruncated_b = (0 != bVal)

                self.m_minValue.Serialize(ar)
                self.m_maxValue.Serialize(ar)

            count = ar.ReadUInt32()
            for i in range(count):
                str = ar.ReadStr()
                self.m_ParametersNames.append(str)

            count = ar.ReadUInt32()
            for i in range(count):
                par = CParameter()
                par.Serialize(ar)
                self.m_Parameters.append(par)

            if bReadCorr:
                nType = ar.ReadInt32()
                if nType < 0:
                    bVal = ar.ReadInt32()
                    self.m_bCorrelated = (0 != bVal)
                else:
                    self.m_bCorrelated = (0 != nType)

                self.m_CorrelatedToName_str = ar.ReadStr()
                for i in range(5):
                    par = CParameter()
                    par.Serialize(ar)
                    self.m_CorrelationConstants.append(par)

            count = ar.ReadUInt32()
            for i in range(count):
                str1 = ar.ReadStr()
                str2 = ar.ReadStr()
                str3 = ar.ReadStr()
                self.m_bootStrapInfo.append((str1, str2, str3))

            self.m_bootStrapSubDist_i32 = ar.ReadInt32()
            b = ar.ReadInt32()
            self.m_multiBootstrap = (0 != b)
            self.m_bootstrapRadius = ar.ReadDbl64()

            #read extra and skip - we don't bother here
            extra = ar.ReadUInt32()

            ar.ReadSkip(extra)

        else:
            pass


class CScalarDistribution(CDistribution):
    def __init__(self):
        super(CScalarDistribution, self).__init__()

    def Serialize(self, ar):
        super(CScalarDistribution, self).Serialize(ar)


class CVectorDistribution(CDistribution):
    def __init__(self):
        super(CVectorDistribution, self).__init__()
        self.m_ParsRad = []
        self.m_localFrameData = CVectorDistLocalFrame()

    def Serialize(self, ar):
        super(CVectorDistribution).Serialize(ar)

        count = ar.ReadUInt32()
        if count:
            for i in range(count):
                self.m_ParsRad.append(ar.ReadDbl64())

        self.m_localFrameData.Serialize(ar)