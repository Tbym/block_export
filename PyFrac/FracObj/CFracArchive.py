import struct
import copy
from mmap import ACCESS_READ, mmap


from CFracArchiveHeader import *
from G3dStyle import *
from GMaterial import *
from G3dScene import *
from G3dContainer import *
from G3dRegion import *
from GProperty import *
from G3dFractureSet import *
from GGrid import *
from G3dSurface import *
from G3dTracemap import *
from G3dWell import *
from WellFractureLog import *
from ParentObject import *
from ResultDataContainer import *
from FractureRefSet import *
from G3dBlockSet import *
from CRockBlock import *
from CBootStrapData import *

objectFactories = dict()

class SClassIOStack:
    def __init__(self):
        self.fileOffset = 0
        self.objVersion = 0
        self.pad = 0
        self.objSize = 0

class CFracArchive:
    def __init__(self, fileIn, isReading):
        self.m_isReading = isReading
        self.m_file = fileIn
        self.m_fileBytes = mmap(fileIn.fileno(), 0, access=ACCESS_READ)
        self.m_curFilePos = 0

        self.m_fileIOStack = []
        self.m_curObjFacts = {}
        self.__curFactInx = 1

        self.head = CFracArchiveHeader()
        self.head.Serialize(self)


#reading functions
    def ReadUInt8(self):
        typeSize = 1
        val = struct.unpack('B', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos+typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadInt8(self):
        typeSize = 1
        val = struct.unpack('b', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadUInt16(self):
        typeSize = 2
        val = struct.unpack('H', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos+typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadInt16(self):
        typeSize = 2
        val = struct.unpack('h', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadUInt32(self):
        typeSize = 4
        val = struct.unpack('I', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadInt32(self):
        typeSize = 4
        val = struct.unpack('i', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadUInt64(self):
        typeSize = 8
        val = struct.unpack('Q', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadInt64(self):
        typeSize = 8
        val = struct.unpack('q', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadFl32(self):
        typeSize = 4
        val = struct.unpack('f', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val
    def ReadDbl64(self):
        typeSize = 8
        val = struct.unpack('d', self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize])[0]
        self.m_curFilePos += typeSize
        return val

    def ReadWStr(self):
        count = self.ReadUInt32()
        typeSize = 2
        if count > 0:
            val = self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize*count].decode('utf-16')
            self.m_curFilePos = self.m_curFilePos + typeSize * count
            return val
        else:
            return ""
    def ReadStr(self):
        count = self.__ReadStringLength()
        typeSize = 1
        if count > 0:
            val = b''.join(struct.unpack('%dc' % count, self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize * count]))
            self.m_curFilePos = self.m_curFilePos + typeSize * count
            return str(val.decode())
        else:
            return ""
    def ReadStr16(self):
        count = self.ReadUInt16()
        typeSize = 1
        if count > 0:
            val = b''.join(struct.unpack('%dc' % count, self.m_fileBytes[self.m_curFilePos:self.m_curFilePos + typeSize * count]))
            self.m_curFilePos = self.m_curFilePos + typeSize * count
            return str(val.decode())
        else:
            return ""

    def __ReadStringLength(self):
        val = self.ReadUInt8()
        if val < int(0xff):
            return val
        val = self.ReadUInt16()
        if val == int(0xfffe):
            val = self.ReadUInt8()
            if val < int(0xff):
                return val
            val = self.ReadUInt16()

        if val < int(0xfffe):
            return val
        val = self.ReadUInt32()
        if val < int(0xffffffff):
            return val
        val = self.ReadUInt64()
        return val
##################################################################################################################

    def GetObjectVersion(self):
        if self.m_fileIOStack:
            return self.m_fileIOStack[-1].objVersion
        else:
            return -1

    def GetObjectSize(self):
        return self.m_fileIOStack[-1].objSize

    def GetArchiveVersion(self):
        return self.head.m_archiveVersion_i32



    def PushArchiveLevel(self):
        curPos = SClassIOStack()
        curPos.fileOffset = self.m_curFilePos
        curPos.objVersion = -1
        curPos.objSize = -1

        self.m_fileIOStack.append(curPos)

    def PopArchiveLevel(self):

        if self.m_isReading:
            self.ReadObjectCompleted()
            self.m_fileIOStack.pop()


    def ReadObjectCompleted(self):

        if len(self.m_fileIOStack) == 0:
            return

        ioStack = self.m_fileIOStack[-1]

        if ioStack.fileOffset == -1 or ioStack.objSize == -1 or ioStack.objVersion == -1:
            return

        self.m_curFilePos = ioStack.fileOffset + ioStack.objSize

    def ReadClassHeader(self):
        if len(self.m_fileIOStack) == 0:
            raise ValueError('no IOstack')
            return

        ioStack = self.m_fileIOStack[-1]
        ioStack.fileOffset = self.m_curFilePos
        flag = self.ReadUInt32()
        if flag != int(0xffffffff):
            raise ValueError('invalid class header found')

        ioStack.objSize = self.ReadUInt64()

        flag = self.ReadUInt32()

        ioStack.objVersion = self.ReadUInt32()

        flag = self.ReadUInt32()
        flag = self.ReadUInt32()

    def ReadClass(self):

        #define tags
        wBigObjectTag = int(0x7FFF)      # 0x7FFF indicates DWORD object tag
        wClassTag = int(0x8000)       # 0x8000 indicates class tag (OR'd)
        wNewClassTag = int(0xFFFF)  # special tag indicating new CRuntimeClass
        dwBigClassTag  = int(0x80000000)  #0x8000000 indicates big class tag (OR'd)

        self.ReadObjectCompleted()
        self.ReadClassHeader()

        keepReading = True

        while keepReading:
            wTag = self.ReadUInt16()
            obTag = 0
            if wTag == wBigObjectTag:
                obTag = self.ReadUInt32()
            else:
                obTag = int(((wTag & wClassTag) << 16) | (wTag & ~wClassTag))

            if wTag == wNewClassTag:
                wclassTag = self.ReadUInt16()
                className = self.ReadStr16()
            else:
                nClassIndex = (obTag & ~dwBigClassTag)
                try:
                    className = self.m_curObjFacts[nClassIndex]
                except:
                    print("Cannot get object from factory - size:{0}, index {1}".format(len(self.m_curObjFacts), nClassIndex))
                    print(self.m_curObjFacts)
                    className = ""

            obj = None
            try:
                obj = globals()[className]()
                print("loading class %s object" % className)
            except:
                print("Cannot create class %s" % className)
            if obj != None:
                if wTag == wNewClassTag:
                    self.m_curObjFacts[self.__curFactInx] = className
                    self.__curFactInx += 1
                return obj
            else:
                print("Class %s is not supported yet" % className)
                #continue reading
                #self.ReadClassHeader()

                #save the class to the container anyway
                if wTag == wNewClassTag:
                    self.m_curObjFacts[self.__curFactInx] = className
                    self.__curFactInx += 1

                #try to read the name if it is a class derived from G3DCore
                dummyObject = G3dCore()
                dummyObject.m_name_str = self.ReadStr() + " ({0}-not supported yet)".format(className)
                dummyObject.m_dummy_b = True

                #skip past the class
                if self.m_fileIOStack[-1].objSize > 0:
                    self.m_curFilePos = self.m_fileIOStack[-1].fileOffset + self.m_fileIOStack[-1].objSize
                #self.m_fileIOStack.pop()
                return dummyObject
                #keepReading = True;
                #
                # obj = None
                # try:
                #     obj = globals()[className]()
                #     print "try create class %s" % className
                # except:
                #     print "Cannot create class %s" % className
                # if obj != None:
                #     self.m_curObjFacts[self.__curFactInx] = className
                #     return obj


    def ReadSkip(self, nBytes):
        self.m_curFilePos = self.m_curFilePos + nBytes