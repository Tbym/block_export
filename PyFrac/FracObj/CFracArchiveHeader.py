
class CFracArchiveHeader:
    def __init__(self):
        self.m_archiveVersion_i32 = 0
        self.m_isTransStor_i32 = True
        self.m_headerSize_ui32 = 0
        self.m_headerVersion_ui32 = 0

        self.archiveType_i32 = 0
        self.licenseID_ui32 = 0
        self.licenseType_ui32 = 0
        self.dongleID_ui32 = 0
        self.spare1_ui32 = 0
        self.spare2_ui32 = 0
        self.fileSource_wstr = ""
        self.spareStr1_wstr = ""
        self.spareStr2_wstr = ""
        self.dateYear_ui16 = 0
        self.dateMonth_ui16 = 0
        self.dateDay_ui16 = 0
        self.dateHour_ui16 = 0

    def Serialize(self, ar):
        if ar.m_isReading:
            self.m_archiveVersion_i32 = ar.ReadInt32()
            self.m_isTransStor_i32 = ar.ReadInt32()
            self.m_headerSize_ui32 = ar.ReadUInt32()
            self.m_headerVersion_ui32 = ar.ReadUInt32()

            self.archiveType_i32 = ar.ReadInt32()
            self.licenseID_ui32 = ar.ReadUInt32()
            self.licenseType_ui32 = ar.ReadUInt32()
            self.dongleID_ui32 = ar.ReadUInt32()
            self.spare1_ui32 = ar.ReadUInt32()
            self.spare2_ui32 = ar.ReadUInt32()

            self.fileSource_wstr = ar.ReadWStr()
            self.spareStr1_wstr = ar.ReadWStr()
            self.spareStr2_wstr = ar.ReadWStr()

            self.dateYear_ui16 = ar.ReadInt16()
            self.dateMonth_ui16 = ar.ReadInt16()
            self.dateDay_ui16 = ar.ReadInt16()
            self.dateHour_ui16 = ar.ReadInt16()

        else:
            self.m_isTransStor_i32 = 0
