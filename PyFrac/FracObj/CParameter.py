

class CParameter(object):
    def __init__(self):
        self.m_paramName_wstr = ""
        self.m_val_f64 = 0.0
        self.m_valueType_i32 = 0
        self.m_valueUnits_i32 = 0
        self.m_paramObj_wstr = ""
        self.m_paramProp_wstr = ""

    def Serialize(self,ar):

        if ar.m_isReading:
            self.m_paramName_wstr = ar.ReadWStr()

            self.m_val_f64 = ar.ReadDbl64()
            self.m_valueType_i32 = ar.ReadInt32()
            self.m_valueUnits_i32 = ar.ReadInt32()

            paramVer = ar.ReadUInt32()
            self.m_paramObj_wstr = ar.ReadWStr()
            self.m_paramProp_wstr = ar.ReadWStr()

            bytesExtra = ar.ReadUInt32()

            if bytesExtra > 0:
                ar.ReadSkip(bytesExtra)
        else:
            pass