from G3dRegion import *
from vec3 import *
from CParameter import *



class FaceData(object):
    def __init__(self):
        self.m_FracID = -1;		#fracture ID
        self.m_area = 0			#area - internal units (m^2)
        self.m_WaterForce = 0	#water force (N)
        self.m_Phi = 0
        self.m_PoreP = 0
        self.m_Cohe = 0
        self.m_Dila = 0
        self.m_JRC = 0
        self.m_JCS = 0
        self.m_CountOuterVs = 0
        self.m_ExternalForce = vec3()
        self.m_outwardNorm = vec3()	#outward normal vector

        self.m_normalMag = 0
        self.m_shearMag = 0
        self.m_strength = 0
        self.m_supportPres = 0



class CRockBlock(G3dPolyhedron):
    def __init__(self):
        super(CRockBlock, self).__init__()

        self.m_density = 0.0
        self.m_model = 0
        self.m_faceData = []
        self.m_visitedFaces = []
        self.m_slidingDir = vec3()
        self.m_boltForce = vec3()
        self.m_load = vec3()
        self.m_failureMode = 0
        self.m_SafetyFactor = 0.0
        self.m_SupportPressure = 0.0
        self.m_ApexHeight = 0.0
        self.m_ApexWidth = 0.0
        self.m_contactArea = 0.0

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            self.m_density = ar.ReadDbl64()
            self.m_model = ar.ReadInt32()

            count = ar.ReadUInt32()

            for i in range(count):
                self.m_faceData[i].m_Phi = ar.ReadDbl64()
                self.m_faceData[i].m_PoreP = ar.ReadDbl64()
                self.m_faceData[i].m_Cohe = ar.ReadDbl64()
                self.m_faceData[i].m_Dila = ar.ReadDbl64()
                self.m_faceData[i].m_JRC = ar.ReadDbl64()
                self.m_faceData[i].m_JCS = ar.ReadDbl64()
            #visited faces
            count = ar.ReadUInt32()
            for i in range(count):
                val = ar.ReadInt32()
                self.m_visitedFaces.append(val)

            self.m_slidingDir[0] = ar.ReadDbl64()
            self.m_slidingDir[1] = ar.ReadDbl64()
            self.m_slidingDir[2] = ar.ReadDbl64()

            count = ar.ReadUInt32()

            for i in range(count):
                self.m_faceData[i].m_normalMag = ar.ReadDbl64()
                self.m_faceData[i].m_shearMag = ar.ReadDbl64()
                self.m_faceData[i].m_strength = ar.ReadDbl64()
                self.m_faceData[i].m_supportPres = ar.ReadDbl64()

    def SerializeComponents(self, ar):
        super(CRockBlock, self).SerializeComponents(ar)

        if ar.m_isReading:
            #serialize CSeismicAcc
            m_accMagnitude = ar.ReadDbl64()
            m_accDirection = vec3()
            m_accDirection[0] = ar.ReadDbl64()
            m_accDirection[1] = ar.ReadDbl64()
            m_accDirection[2] = ar.ReadDbl64()

            self.m_boltForce[0] = ar.ReadDbl64()
            self.m_boltForce[1] = ar.ReadDbl64()
            self.m_boltForce[2] = ar.ReadDbl64()

            self.m_load[0] = ar.ReadDbl64()
            self.m_load[1] = ar.ReadDbl64()
            self.m_load[2] = ar.ReadDbl64()

            self.m_volume_f64 = ar.ReadDbl64()
            if ar.GetArchiveVersion() >= 730:
                self.m_failureMode = ar.ReadInt32()

                self.m_SafetyFactor = ar.ReadDbl64()
                self.m_SupportPressure = ar.ReadDbl64()
                self.m_ApexHeight = ar.ReadDbl64()
                self.m_ApexWidth = ar.ReadDbl64()
            if ar.GetArchiveVersion() >= 777:
                self.m_contactArea = ar.ReadDbl64()

            #read facedata
            count = ar.ReadUInt32()
            self.m_faceData = []
            for i in range(count):
                face = FaceData()
                face.m_FracID = ar.ReadInt32()
                face.m_area = ar.ReadDbl64()
                face.m_WaterForce = ar.ReadDbl64()

                if ar.GetObjectVersion() >= 3:
                    face.m_ExternalForce[0] = ar.ReadDbl64()
                    face.m_ExternalForce[1] = ar.ReadDbl64()
                    face.m_ExternalForce[2] = ar.ReadDbl64()

                face.m_outwardNorm[0] = ar.ReadDbl64()
                face.m_outwardNorm[1] = ar.ReadDbl64()
                face.m_outwardNorm[2] = ar.ReadDbl64()

                self.m_faceData.append(face)

            if ar.GetObjectVersion() >= 7:
                #load stress tensor
                for i in range(7):
                    c = CParameter()
                    c.Serialize(ar)
