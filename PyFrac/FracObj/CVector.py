
class CVectorDistLocalFrame(object):
    def __init__(self):

        self.m_locFrameObject_str = ""
        self.m_locXTrendProp_str = ""
        self.m_locXPlungeProp_str = ""
        self.m_locYTrendProp_str = ""
        self.m_locYPlungeProp_str = ""
        self.m_locZTrendProp_str = ""
        self.m_locZPlungeProp_str = ""



    def Serialize(self,ar):
        bVal = ar.ReadInt32()
        if bVal != 0:
            bVal = ar.ReadInt32()
            if ar.GetArchiveVersion() > 747:
                bDefX = (bVal != 0)
                bVal = ar.ReadInt32()
                bDefY = (bVal != 0)
                bVal = ar.ReadInt32()
                bDefZ = (bVal != 0)

                self.m_locFrameObject_str = ar.ReadStr()

                if bDefX:
                    self.m_locXTrendProp_str = ar.ReadStr()
                    self.m_locYPlungeProp_str = ar.ReadStr()
                if bDefY:
                    self.m_locYTrendProp_str = ar.ReadStr()
                    self.m_locYPlungeProp_str = ar.ReadStr()
                if bDefZ:
                    self.m_locZTrendProp_str = ar.ReadStr()
                    self.m_locZPlungeProp_str = ar.ReadStr()

