import os
import sys
sys.path.append((os.path.dirname(os.path.realpath(__file__))))

from G3dRegion import *
from PolyTri import *
import os

def ExportSurfToTS(outFileName, surfaceObj, pOrigin = vec3(), inFeet=False):

    ftToM = 0.3048
    if inFeet:
        pOrigin = pOrigin / ftToM

    try:
        with open(outFileName, 'w') as outfile:
            # Write Header...
            outfile.write('GOCAD TSurf 1\n')
            outfile.write('HEADER {\n')
            outfile.write("name:{0}\n".format(surfaceObj.m_name_str))
            outfile.write('}\n')

            # Write data...
            outfile.write('TFACE\n')
            for i, p in enumerate(surfaceObj.m_Polyhedron.m_Points):
                if inFeet:
                    p = p / ftToM


                outfile.write("VRTX {0} {1: >9.4f} {2: >9.4f} {3: >9.4f}\n".format(i+1, p.X() + pOrigin.X(), p.Y() + pOrigin.Y(), p.Z() + pOrigin.Z()))
            for f in surfaceObj.m_Polyhedron.m_Faces:
                if len(f.m_PointIds) == 3:
                    outfile.write('TRGL {0} {1} {2}\n'.format(f.m_PointIds[0] + 1, f.m_PointIds[1] + 1, f.m_PointIds[2] + 1))
                elif len(f.m_PointIds) == 4:
                    outfile.write('TRGL {0} {1} {2}\n'.format(f.m_PointIds[0] + 1, f.m_PointIds[1] + 1, f.m_PointIds[2] + 1))
                    outfile.write('TRGL {0} {1} {2}\n'.format(f.m_PointIds[0] + 1, f.m_PointIds[2] + 1, f.m_PointIds[3] + 1))
                else:
                    points = [surfaceObj.m_Polyhedron.m_Points[x] for x in f.m_PointIds]
                    polygon = np.array(points)
                    triItt = triangulate(polygon)
                    for tri in triItt:
                        v = vec3(tri[0])
                        i1 = surfaceObj.m_Polyhedron.m_Points.index(v)
                        v = vec3(tri[1])
                        i2 = surfaceObj.m_Polyhedron.m_Points.index(v)
                        v = vec3(tri[2])
                        i3 = surfaceObj.m_Polyhedron.m_Points.index(v)
                        outfile.write('TRGL {0} {1} {2}\n'.format(i1 + 1, i2 + 1, i3 + 1))



            outfile.write('END\n')

        print("Surface exported to {0}".format(outFileName))
        return True
    except:
        print("ERROR exporting surface {0}".format(outFileName))
        #delete file
        os.remove(outFileName)
