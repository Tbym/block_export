import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from ModelG3d import *
from CDefinition import *
from CFracArchive import *


#CG3dDoc
class FracManFile(object):
    def __init__(self):
        #super(G3dCore, self).__init__()
        self.m_ModelG3d = ModelG3d()
        self.m_ModelDef = ModelObjectDefinition()
        self.m_curFileInfo = CFracArchiveHeader()

    def Open(self, fileName):
        with open(fileName, "rb") as f:
            ar = CFracArchive(f, True)
            self.m_ModelG3d.Serialize(ar)
            #don't load the definitions
            #self.m_ModelDef.Serialize(ar)

    def GetObjectsList(self, objTypes = None):
        names = []
        parentName =""
        self.m_ModelG3d.m_scene.GetChildNameList(names, parentName, objTypes)
        return names

    def GetObjectByName(self, objName):

        if objName.startswith("/Scene/"):
            tempStr = objName.split("/")
            name = tempStr[-1]
        else:
            name = objName

        return self.m_ModelG3d.m_scene.GetObjectByName(name)
