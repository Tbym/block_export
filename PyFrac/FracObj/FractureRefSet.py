from G3dFractureSet import *

class FractureRef(object):
    def __init__(self):
        self.m_FIndex_ui32 = 0
        self.m_faceIndex_ui32 = 0
        self.m_pSet = ""



class FractureRefSet(G3dCore):
    def __init__(self):
        super(FractureRefSet, self).__init__()

        self.m_tempRefs = []
        self.m_type_i32 = 0
        self.m_refProperties = GProperties()
        self.m_length_f32 = 0.0


    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.GetObjectVersion()>=3:
            if ar.m_isReading:
                count = ar.ReadUInt32()
                for i in range(count):
                    self.m_tempRefs[i].m_faceIndex_ui32 = ar.ReadUInt32()


    def SerializeAttributes(self, ar):
        super(FractureRefSet, self).SerializeAttributes(ar)

    def SerializeComponents(self, ar):
        if ar.m_isReading:
            fc = ar.ReadUInt32()

            for i in range(fc):
                si = ar.ReadUInt32()
                fi = ar.ReadUInt32()
                f = FractureRef()
                f.m_pSet = si
                f.m_FIndex_ui32 = fi
                self.m_tempRefs.append(f)


            self.m_type_i32 = ar.ReadInt32()
            percentDisplay = ar.ReadFl32()
            self.m_length_f32 = ar.ReadFl32()

            self.m_refProperties.Serialize(ar)