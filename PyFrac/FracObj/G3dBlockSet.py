from G3dCore import *


class G3dBlockSet(G3dCore):
    def __init__(self):
        super(G3dBlockSet, self).__init__()

        self.m_wedgeDefinitionName = ""

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)
        if ar.m_isReading:
            self.m_wedgeDefinitionName = ar.ReadStr()

    def GetBlock(self, id):
        if id < len(self.m_children):
            return self.m_children[id]
        else:
            return None

    def GetBlockProperty(self, id, propName):
        if id < len(self.m_children):

            return self.m_childProperties.GetPropValue(id, propName)
        else:
            return None