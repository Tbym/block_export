from G3dCore import *

class G3dContainer(G3dCore):
    def __init__(self):
        self.m_type_i32 = 0
        super(G3dContainer, self).__init__()

    def Serialize(self,ar):
        super(G3dContainer, self).Serialize(ar) #call G3dCore serialization

    def SerializeAttributes(self, ar):
        super(G3dContainer, self).SerializeAttributes(ar)  # call G3dCore serialization
        if ar.m_isReading:
            self.m_type_i32 = ar.ReadInt32()
        else:
            pass