from GProperties import *
from G3dStyle import *

class G3dCore(object):
    def __init__(self):
        self.m_children = []
        self.m_childProperties = GProperties()
        self.m_properties = []
        self.m_style = G3dStyle()
        self.m_parent = None
        self.m_index = 0
        self.m_name_str = ""
        self.m_comments_wstr = ""
        self.m_sourceFile_wstr = ""
        #use this for classes that are not implemented yet
        self.m_dummy_b = False


    def Serialize(self,ar):

        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

    def SerializeAttributes(self, ar):
        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_style.Serialize(ar)
            self.m_comments_wstr = ar.ReadWStr()
            self.m_sourceFile_wstr = ar.ReadWStr()


    def SerializeComponents(self, ar):
        pass

    def SerializeChildren(self, ar):

        self.m_childProperties.Serialize(ar)

        if ar.m_isReading:
            count = ar.ReadUInt32()
            if count > 0:
                ar.PushArchiveLevel()

                for i in range(count):
                    child = ar.ReadClass()
                    if child != None:
                        if isinstance(child, G3dCore):
                            if child.m_dummy_b == False:
                                child.Serialize(ar)
                        else:
                            child.Serialize(ar)
                        child.m_parent = self
                        self.m_children.append(child)
                        child.m_index = len(self.m_children)-1
                ar.PopArchiveLevel()
        else:
            pass

    def GetChildNameList(self, names, parentNames, objTypes = None):

        if len(self.m_children)>0:
            parName = parentNames + "/" + self.m_name_str

            for o in self.m_children:
                if isinstance(o,G3dCore):
                    o.GetChildNameList(names, parName, objTypes)

        else:
            if objTypes == None or isinstance(self, objTypes):
                names.append(parentNames + "/" + self.m_name_str)


    def GetObjectByName(self, objName):

        if objName == self.m_name_str:
            return self

        elif len(self.m_children) > 0:
            obj = None
            for o in self.m_children:
                if isinstance(o, G3dCore):
                    obj = o.GetObjectByName(objName)
                if obj != None:
                    break
            return obj
        return None


    def GetOrigin(self):
        return self.m_parent.GetOrigin()