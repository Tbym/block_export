from G3dSurface import *
from vec3 import *

class G3dFractureSetBase(G3dCore):
    def __init__(self):
        super(G3dFractureSetBase, self).__init__()

class Fracture(object):
    def __init__(self):
        self.m_Count_ui32 = 0
        self.m_V = []



class G3dFractureSet(G3dFractureSetBase):
    def __init__(self):
        super(G3dFractureSet, self).__init__()
        self.m_setDefName_str = ""
        self.m_Fractures = []
        self.m_fracProperties = GProperties()
        self.m_origin = vec3()
        self.m_FracSetType_i32 = 0
        self.m_PercentDisplayed_f32 = 0.0
        self.m_thickProperty_str = ""
        self.m_generationSeed_i64 = 0
        self.m_visibleProp_str = ""


    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            legVal = ar.ReadUInt32()
            self.m_visibleProp_str = ar.ReadStr()
            self.m_generationSeed_i64 = ar.ReadInt64()
        else:
            pass

    def SerializeAttributes(self, ar):
        super(G3dFractureSet,self).SerializeAttributes(ar)

        if ar.m_isReading:
            self.m_setDefName_str = ar.ReadStr()
            self.m_FracSetType_i32 = ar.ReadInt32()
            self.m_PercentDisplayed_f32 = ar.ReadFl32()
            self.m_fracProperties.Serialize(ar)
            self.m_thickProperty_str = ar.ReadStr()
        else:
            pass

    def SerializeComponents(self, ar):

        self.m_origin[0] = ar.ReadDbl64()
        self.m_origin[1] = ar.ReadDbl64()
        self.m_origin[2] = ar.ReadDbl64()

        fc = ar.ReadUInt32()

        for i in range(fc):
            f = Fracture()
            f.m_Count_ui32 = ar.ReadUInt32()

            for j in range(f.m_Count_ui32):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                f.m_V.append(v)

            self.m_Fractures.append(f)

            if ar.GetObjectVersion()>= 5:
                #read 3 floats m_vSeed
                ar.ReadDbl64()
                ar.ReadDbl64()
                ar.ReadDbl64()


class DeterministicFracSet(G3dFractureSetBase):
    def __init__(self):
        super(DeterministicFracSet, self).__init__()
        self.m_setDefName_str = ""
        self.m_internalFracs = []
        self.m_fracProperties = GProperties()
        self.m_origin = vec3()
        self.m_FracSetType_i32 = 0
        self.m_PercentDisplayed_f32 = 0.0
        self.m_thickProperty_str = ""
        self.m_generationSeed_i64 = 0
        self.m_visibleProp_str = ""
        self.m_storeInternal_b = True

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            self.m_storeInternal_b = ar.ReadUInt8()
            count = ar.ReadUInt32()
            for i in range(count):
                surf = G3dSurface()
                surf.SerializeContained()
            if ar.GetObjectVersion() <= 2:
                self.m_generationSeed_i64 = ar.ReadUInt32()
            else:
                self.m_generationSeed_i64 = ar.ReadUInt64()
        else:
            pass

    def SerializeAttributes(self, ar):
        super(DeterministicFracSet,self).SerializeAttributes(ar)

        if ar.m_isReading:
            self.m_setDefName_str = ar.ReadStr()
            self.m_FracSetType_i32 = ar.ReadInt32()
            self.m_PercentDisplayed_f32 = ar.ReadFl32()
            self.m_fracProperties.Serialize(ar)
            self.m_thickProperty_str = ar.ReadStr()
        else:
            pass

    def SerializeComponents(self, ar):
        super(DeterministicFracSet, self).SerializeComponents(ar)

    def SerializeChildren(self, ar):
        super(DeterministicFracSet, self).SerializeChildren(ar)