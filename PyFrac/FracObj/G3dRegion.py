from G3dCore import *
from Geometry import *
from vec3 import *

class G3dRegion(G3dCore):
    def __init__(self):
        self.m_volume_f64 = 0
        super(G3dRegion, self).__init__()

    def SerializeAttributes(self,ar):
        super(G3dRegion, self).SerializeAttributes(ar) #call G3dCore serialization

        if ar.m_isReading:
            self.m_volume_f64 = ar.ReadDbl64()
        else:
            pass




class G3dPolyhedron(G3dRegion):
    def __init__(self):
        self.m_Polyhedron = Polyhedron()
        self.m_basePickInx_ui32 = 0
        super(G3dPolyhedron, self).__init__()

    def SerializeComponents(self, ar):

        if ar.m_isReading:

            super(G3dPolyhedron, self).SerializeComponents(ar)  # call G3dCore serialization

            tmp = ar.ReadUInt32()
            self.m_Polyhedron.m_Type = tmp

            numOfVerts = ar.ReadUInt32()

            for i in range(numOfVerts):
                p = vec3()
                p[0] = ar.ReadDbl64()
                p[1] = ar.ReadDbl64()
                p[2] = ar.ReadDbl64()
                self.m_Polyhedron.m_Points.append(p)

            numOfFaces = ar.ReadUInt32()

            for i in range(numOfFaces):
                pFace = Polygon3DPtr()

                numOfPts = ar.ReadUInt32()

                for j in range(numOfPts):
                    idx = ar.ReadInt32()
                    if idx < len(self.m_Polyhedron.m_Points):
                        #pFace.m_Points.append(self.m_Polyhedron.m_Points[idx])
                        pFace.m_PointIds.append(idx)
                    else:
                        print("Invalid object")

                if len(pFace.m_PointIds) >= 3:
                    self.m_Polyhedron.m_Faces.append(pFace)
        else:
            pass


class G3dRegionPoly(G3dPolyhedron):
    def __init__(self):
        self.m_Shape_i32 = 0
        self.m_BasePolygon = Polygon2DPtr()
        self.m_AxisLength_f64 = 0.0
        self.m_Transform = Matrix4x4()
        self.m_Trend_f64 = 0.0
        self.m_Plunge_f64 = 0.0
        self.m_ScanTrend_f64 = 0.0
        self.m_ScanPlunge_f64 = 0.0
        self.m_TranTrend_f64 = 0.0
        self.m_TranPlunge_f64 = 0.0
        self.m_HighTrend_f64 = 0.0
        self.m_HighPlunge_f64 = 0.0
        self.m_Size = vec3()
        self.m_NumPanels_i32 = 0
        self.m_RadiusWidth_f64 = 0.0
        self.m_WallHeight_f64 = 0.0
        self.m_ArchHeight_f64 = 0.0
        super(G3dRegionPoly, self).__init__()

    def Serialize(self, ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

    def SerializeComponents(self, ar):
        super(G3dRegionPoly, self).SerializeComponents(ar)

        if ar.m_isReading:
            self.m_Shape_i32 = ar.ReadInt32()
            bc = ar.ReadUInt32()

            for i in range(bc):
                p = vec3()
                p[0] = ar.ReadDbl64()
                p[1] = ar.ReadDbl64()
                p[2] = 0
                self.m_BasePolygon.m_Points.append(p)

            self.m_AxisLength_f64 = ar.ReadDbl64()

            for i in range(16):
                self.m_Transform.m_Values.append(ar.ReadDbl64())

            self.m_Trend_f64 = ar.ReadDbl64()
            self.m_Plunge_f64 = ar.ReadDbl64()
            self.m_ScanTrend_f64 = ar.ReadDbl64()
            self.m_ScanPlunge_f64 = ar.ReadDbl64()
            self.m_TranTrend_f64 = ar.ReadDbl64()
            self.m_TranPlunge_f64 = ar.ReadDbl64()
            self.m_HighTrend_f64 = ar.ReadDbl64()
            self.m_HighPlunge_f64 = ar.ReadDbl64()

            self.m_Size[0] = ar.ReadDbl64()
            self.m_Size[1] = ar.ReadDbl64()
            self.m_Size[2] = ar.ReadDbl64()

            self.m_NumPanels_i32 = ar.ReadInt32()
            self.m_RadiusWidth_f64 = ar.ReadDbl64()
            self.m_WallHeight_f64 = ar.ReadDbl64()

            gc = ar.ReadUInt32()
            for i in range(gc):
                str = ar.ReadStr()

            ar.ReadInt32()
        else:
            pass