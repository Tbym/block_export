
from G3dCore import *
from GMaterial import *
from vec3 import *

class G3dScene(G3dCore):
    def __init__(self):
        self.m_viewScaleX_f64 = 0.0
        self.m_viewScaleY_f64 = 0.0
        self.m_viewScaleZ_f64 = 0.0
        self.m_clearColor = GMaterial()
        self.m_clearColor2 = GMaterial()
        self.m_fracWorldRegion_str = ""
        self.m_originX = 0.0
        self.m_originY = 0.0
        self.m_originZ = 0.0
        super(G3dScene, self).__init__()

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

    def SerializeAttributes(self, ar):

        super(G3dScene,self).SerializeAttributes(ar)

        if(ar.m_isReading):
            self.m_viewScaleX_f64 = ar.ReadDbl64()
            self.m_viewScaleY_f64 = ar.ReadDbl64()
            self.m_viewScaleZ_f64 = ar.ReadDbl64()
            self.m_clearColor.Serialize(ar)
            self.m_clearColor2.Serialize(ar)
            legScale = ar.ReadInt32()
            legScale = ar.ReadInt32()
            legScale = ar.ReadInt32()
            self.m_fracWorldRegion_str = ar.ReadStr()
            inx = ar.ReadInt32()
            self.m_originX = ar.ReadDbl64()
            self.m_originY = ar.ReadDbl64()
            self.m_originZ = ar.ReadDbl64()

    def GetOrigin(self):
        return vec3(self.m_originX, self.m_originY, self.m_originZ)
