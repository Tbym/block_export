from GMaterial import *

class G3dStyle(object):
    def __init__(self):
        self.m_bools_ui32 = 0
        self.m_floats = []
        self.m_Materials = []
        self.m_nFloats = 4
        self.m_nMaterials = 6

    def Serialize(self, ar):
        if ar.m_isReading:
            self.m_bools_ui32 = ar.ReadUInt32()

            for i in range(self.m_nFloats):
                self.m_floats.append(ar.ReadFl32())

            for i in range(self.m_nMaterials):
                mat = GMaterial()
                mat.Serialize(ar)
                self.m_Materials.append(mat)
        else:
            pass