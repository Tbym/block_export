from G3dCore import *
from GTextureMap import *
from GArrows import *
from vec3 import *

class Triangle(object):
    def __init__(self):
        self.m_V = [0,0,0]

class G3dSurface(G3dCore):
    def __init__(self):
        super(G3dSurface, self).__init__()
        self.m_InvalidCoordVal_f64 = 0.0
        self.m_origin = vec3()

        self.m_verts = []
        self.m_tris = []
        self.m_VNormals = []
        self.m_pTextureMaps = []
        self.m_arrows = GArrows()

        self.m_thickProperty_str = ""
        self.m_textPropU_str = ""
        self.m_textPropV_str = ""

        self.m_VertProps = GProperties()
        self.m_TriProps = GProperties()


    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            self.m_origin[0] = ar.ReadDbl64()
            self.m_origin[1] = ar.ReadDbl64()
            self.m_origin[2] = ar.ReadDbl64()

            vc = ar.ReadUInt32()
            #delete the list
            self.m_verts = []
            for i in range(vc):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                self.m_verts.append(v)

            self.m_arrows.SerializedContained(ar)

            ar.ReadStr()
            ar.ReadStr()


    def SerializeContained(self, ar):
        ar.PushArchiveLevel()
        if ar.m_isReading:
            o = ar.ReadClass()
            if isinstance(o,G3dSurface):
                self.Serialize(ar)
        ar.PopArchiveLevel()

    def SerializeComponents(self, ar):
        if ar.m_isReading:
            if ar.GetArchiveVersion() >= 732:
                self.m_InvalidCoordVal_f64 = ar.ReadDbl64()

                vc = ar.ReadUInt32()
                if vc > 0:
                    vOrg = vec3()
                    vOrg[0] = ar.ReadDbl64()
                    vOrg[1] = ar.ReadDbl64()
                    vOrg[2] = ar.ReadDbl64()

                    for i in range(vc):
                        v = vec3()
                        v[0] = ar.ReadDbl64()
                        v[1] = ar.ReadDbl64()
                        v[2] = ar.ReadDbl64()
                        self.m_verts.append(v - vOrg)

                tc = ar.ReadUInt32()
                for i in range(tc):
                    t = Triangle()
                    t.m_V[0] = ar.ReadUInt32()
                    t.m_V[1] = ar.ReadUInt32()
                    t.m_V[2] = ar.ReadUInt32()
                    self.m_tris.append(t)

                nc = ar.ReadUInt32()
                for i in range(nc):
                    v = vec3()
                    v[0] = ar.ReadFl32()
                    v[1] = ar.ReadFl32()
                    v[2] = ar.ReadFl32()
                    self.m_VNormals.append(v)

                self.m_VertProps.Serialize(ar)
                self.m_TriProps.Serialize(ar)

                if ar.GetArchiveVersion() >= 762:
                    self.m_thickProperty_str = ar.ReadStr()

                if ar.GetArchiveVersion() >= 764:
                    tc = ar.ReadUInt32()
                    for i in range(tc):
                        texture = GTextureMap()
                        texture.Serialize(ar)
                        self.m_pTextureMaps.append(texture)

                    self.m_textPropU_str = ar.ReadStr()
                    self.m_textPropV_str = ar.ReadStr()


