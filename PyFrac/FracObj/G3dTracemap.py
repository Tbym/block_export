from G3dCore import *
from vec3 import *


class G3dTracemap(G3dCore):
    def __init__(self):
        super(G3dTracemap, self).__init__()
        #self.m_InvalidCoordVal_f64 = 0.0
        self.m_origin = vec3()
        self.m_surfaceName_str = ""
        self.m_verts = []
        self.m_traces = []

        self.m_VertsProps = GProperties()
        self.m_SegProps = GProperties()
        self.m_TraceProps = GProperties()

    def Serialize(self,ar):
        if ar.m_isReading:

            self.m_name_str = ar.ReadStr()
            self.m_style.Serialize(ar)

            self.m_comments_wstr = ar.ReadWStr()
            self.m_sourceFile_wstr = ar.ReadWStr()

            e = ar.ReadInt32()
            e = ar.ReadInt32()

            self.m_surfaceName_str = ar.ReadStr()

            self.m_origin[0] = ar.ReadDbl64()
            self.m_origin[1] = ar.ReadDbl64()
            self.m_origin[2] = ar.ReadDbl64()

            vc = ar.ReadUInt32()
            # delete the list
            self.m_verts = []
            for i in range(vc):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                self.m_verts.append(v)

            count = ar.ReadUInt32()
            for i in range(count):
                segVtxCount = ar.ReadUInt32()
                segs = []
                for j in range(segVtxCount):
                    segs.append(ar.ReadUInt32())
                self.m_traces.append(segs)

            self.m_VertsProps.Serialize(ar)
            self.m_SegProps.Serialize(ar)
            self.m_TraceProps.Serialize(ar)


        self.SerializeChildren(ar)