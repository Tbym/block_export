from G3dCore import *
from vec3 import *


class SWellFlowInfo(object):
    def __init__(self):
        self.rate_f64 = 0.0
        self.duration_f64 = 0.0
        self.formationValueFactor_f64 = 0.0

class G3dWell(G3dCore):
    def __init__(self):
        super(G3dWell, self).__init__()

        self.m_origin = vec3()
        self.m_wellTrajectory = []
        self.m_vertexProps = GProperties()
        self.basePickInx_ui32 = 0
        self.m_depthMarkIncrement_f64 = 0.0
        self.m_depthMarkSize_f64 = 0.0
        self.m_RigHeight_f64 = 0.0
        self.m_LabelStringDepth_f64 = 0.0
        self.m_DepthLabelType_i32 = 0
        self.m_wellFlow = []

    def Serialize(self,ar):
        super(G3dWell, self).Serialize(ar)

    def SerializeAttributes(self, ar):

        super(G3dWell, self).SerializeAttributes(ar)

        if (ar.m_isReading):
            self.m_depthMarkIncrement_f64 = ar.ReadDbl64()
            self.m_depthMarkSize_f64 = ar.ReadDbl64()

            val = ar.ReadInt32()
            val = ar.ReadInt32()

            self.m_LabelStringDepth_f64 = ar.ReadDbl64()
            self.m_RigHeight_f64 = ar.ReadDbl64()

            val = ar.ReadInt32()

            self.m_DepthLabelType_i32 = ar.ReadInt32()

            self.m_origin[0] = ar.ReadDbl64()
            self.m_origin[1] = ar.ReadDbl64()
            self.m_origin[2] = ar.ReadDbl64()

            count = ar.ReadUInt32()
            # delete the list
            self.m_wellTrajectory = []
            for i in range(count):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                self.m_wellTrajectory.append(v)

            self.m_vertexProps.Serialize(ar)

            self.m_wellFlow = []

            count = ar.ReadUInt32()
            for i in range(count):
                wf = SWellFlowInfo()
                wf.duration_f64 = ar.ReadDbl64()
                wf.rate_f64= ar.ReadDbl64()
                wf.formationValueFactor_f64 = ar.ReadDbl64()
                self.m_wellFlow.append(wf)