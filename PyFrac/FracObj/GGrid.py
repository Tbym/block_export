
from G3dCore import *
from GArrows import *
from vec3 import *


class Cell(object):
    def __init__(self):
        self.m_vIdx_i32 = [-1,-1,-1,-1,-1,-1,-1,-1]
        self.m_vCount_ui32 = 0

class GridFacePoly(object):
    def __init__(self):
        self.m_cellInx_ui32 = 0
        self.m_faceInx_ui8 = 0
        self.m_flags_ui16 = 0
        self.m_spare_ui8 = 0

class GGrid(G3dCore):
    def __init__(self):
        super(GGrid, self).__init__()
        self.m_bRegularGrid_i32 = 0
        self.m_numCells_i32 = [0,0,0]
        self.m_nullValue_f32 = 0.0
        self.m_PlaneDir_i32 = 0
        self.m_PlaneIndex_i32 = 0
        self.m_CellIndices_i32 = [0,0,0]
        self.m_gridDrawType_i32 = 0
        self.m_subsetFunction_str = ""

        self.m_vertProperties = GProperties()
        self.m_cellProperties = GProperties()
        self.m_origin = vec3()
        self.m_vertices = []
        self.m_cells = []
        self.m_outerFaces = []

        self.m_arrows = GArrows()

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            if ar.GetObjectVersion() >= 2:
                self.m_gridDrawType_i32 = ar.ReadInt32()

                self.m_PlaneDir_i32 = ar.ReadInt32()
                self.m_PlaneIndex_i32 = ar.ReadInt32()
                self.m_CellIndices_i32[0] = ar.ReadInt32()
                self.m_CellIndices_i32[1] = ar.ReadInt32()
                self.m_CellIndices_i32[2] = ar.ReadInt32()
                self.m_subsetFunction_str = ar.ReadStr()

                outerFaceCount = ar.ReadUInt32()
                if outerFaceCount:
                    for i in range(outerFaceCount):
                        gr = GridFacePoly()
                        gr.m_cellInx_ui32 = ar.ReadUInt32()
                        gr.m_faceInx_ui8 = ar.ReadUInt8()
                        gr.m_flags_ui16 = ar.ReadUInt16()
                        gr.m_spare_ui8 = ar.ReadUInt8()
                        dummy = ar.ReadUInt32()

                        self.m_outerFaces.append(gr)

                if ar.GetObjectVersion() >= 3:
                    self.m_arrows.SerializedContained(ar)
                if ar.GetObjectVersion() >= 4:
                    self.m_PlaneCount_i32 = ar.ReadInt32()





    def SerializeComponents(self, ar):
        self.SerializeVertexComponents(ar)

        if ar.m_isReading:

            self.m_bRegularGrid_i32 = ar.ReadInt32()
            bReadFaces = False
            if self.m_bRegularGrid_i32 == -1:
                self.m_bRegularGrid_i32 = True
                bReadFaces = True

            self.m_numCells_i32[0] = ar.ReadInt32()
            self.m_numCells_i32[1] = ar.ReadInt32()
            self.m_numCells_i32[2] = ar.ReadInt32()

            self.m_nullValue_f32 = ar.ReadFl32()

            self.SerializeCells(ar)

    def SerializeVertexComponents(self, ar):

        super(GGrid, self).SerializeComponents(ar)

        self.SerializeVertices(ar)

    def SerializeCells(self, ar):

        self.m_cellProperties.Serialize(ar)

        if ar.m_isReading:
            count = ar.ReadUInt32()

            for i in range(count):
                c = Cell()
                for j in range(8): #8 indices
                    c.m_vIdx_i32[j] = ar.ReadInt32()
                c.m_vCount_ui32 = ar.ReadInt32()
                self.m_cells.append(c)



    def SerializeVertices(self, ar):
        self.m_vertProperties.Serialize(ar)

        if ar.m_isReading:

            self.m_origin[0] = ar.ReadDbl64()
            self.m_origin[1] = ar.ReadDbl64()
            self.m_origin[2] = ar.ReadDbl64()

            count = ar.ReadUInt32()

            for i in range(count):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                self.m_vertices.append(v)

class RegularGrid(G3dCore):
    def __init__(self):
        super(RegularGrid, self).__init__()
        self.m_bRegularGrid_i32 = 0
        self.m_numCells_i32 = [0,0,0]
        self.m_nullValue_f32 = 0.0
        self.m_PlaneDir_i32 = 0
        self.m_PlaneIndex_i32 = 0
        self.m_CellIndices_i32 = [0,0,0]
        self.m_gridDrawType_i32 = 0
        self.m_subsetFunction_str = ""

        self.m_vertProperties = GProperties()
        self.m_cellProperties = GProperties()
        self.m_origin = vec3()
        self.m_vertices = []
        self.m_cells = []
        self.m_outerFaces = []

        self.m_arrows = GArrows()

    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            if ar.GetObjectVersion() >= 2:
                self.m_gridDrawType_i32 = ar.ReadInt32()

                self.m_PlaneDir_i32 = ar.ReadInt32()
                self.m_PlaneIndex_i32 = ar.ReadInt32()
                self.m_CellIndices_i32[0] = ar.ReadInt32()
                self.m_CellIndices_i32[1] = ar.ReadInt32()
                self.m_CellIndices_i32[2] = ar.ReadInt32()
                self.m_subsetFunction_str = ar.ReadStr()

                outerFaceCount = ar.ReadUInt32()
                if outerFaceCount:
                    for i in range(outerFaceCount):
                        gr = GridFacePoly()
                        gr.m_cellInx_ui32 = ar.ReadUInt32()
                        gr.m_faceInx_ui8 = ar.ReadUInt8()
                        gr.m_flags_ui16 = ar.ReadUInt16()
                        gr.m_spare_ui8 = ar.ReadUInt8()
                        dummy = ar.ReadUInt32()

                        self.m_outerFaces.append(gr)

                if ar.GetObjectVersion() >= 3:
                    self.m_arrows.SerializedContained(ar)
                if ar.GetObjectVersion() >= 4:
                    self.m_PlaneCount_i32 = ar.ReadInt32()





    def SerializeComponents(self, ar):
        self.SerializeVertexComponents(ar)

        if ar.m_isReading:

            self.m_bRegularGrid_i32 = ar.ReadInt32()
            bReadFaces = False
            if self.m_bRegularGrid_i32 == -1:
                self.m_bRegularGrid_i32 = True
                bReadFaces = True

            self.m_numCells_i32[0] = ar.ReadInt32()
            self.m_numCells_i32[1] = ar.ReadInt32()
            self.m_numCells_i32[2] = ar.ReadInt32()

            self.m_nullValue_f32 = ar.ReadFl32()

            self.SerializeCells(ar)

    def SerializeVertexComponents(self, ar):

        super(RegularGrid, self).SerializeComponents(ar)

        self.SerializeVertices(ar)

    def SerializeCells(self, ar):

        self.m_cellProperties.Serialize(ar)

        if ar.m_isReading:
            count = ar.ReadUInt32()

            for i in range(count):
                c = Cell()
                for j in range(8): #8 indices
                    c.m_vIdx_i32[j] = ar.ReadInt32()
                c.m_vCount_ui32 = ar.ReadInt32()
                self.m_cells.append(c)



    def SerializeVertices(self, ar):
        self.m_vertProperties.Serialize(ar)

        if ar.m_isReading:

            self.m_origin[0] = ar.ReadDbl64()
            self.m_origin[1] = ar.ReadDbl64()
            self.m_origin[2] = ar.ReadDbl64()

            count = ar.ReadUInt32()

            for i in range(count):
                v = vec3()
                v[0] = ar.ReadFl32()
                v[1] = ar.ReadFl32()
                v[2] = ar.ReadFl32()
                self.m_vertices.append(v)




