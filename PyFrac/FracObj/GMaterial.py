
class GMaterial:
    def __init__(self):
        self.m_Values_4f32 = []
        self.m_specular_f32 = 0.0
        self.m_spare_f32 = 0.0

    def Serialize(self, ar):
        if (ar.m_isReading):
            #read argb
            self.m_Values_4f32.append(ar.ReadFl32())
            self.m_Values_4f32.append(ar.ReadFl32())
            self.m_Values_4f32.append(ar.ReadFl32())
            self.m_Values_4f32.append(ar.ReadFl32())

            self.m_specular_f32 = ar.ReadFl32()
            self.m_spare_f32 = ar.ReadFl32()
        else:
            pass