
class GProperties(object):
    def __init__(self):
        self.m_properties = []

    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_properties = []

            count = ar.ReadUInt32()

            if count > 0:
                ar.PushArchiveLevel()

                for i in range(count):
                    child = ar.ReadClass()
                    if child != None:
                        child.Serialize(ar)
                        child.m_parent = self
                        self.m_properties.append(child)
                ar.PopArchiveLevel()

        else:
            pass

    def GetPropValue(self, id, propName):

        for prop in self.m_properties:
            if prop.m_name_str == propName:
                return prop.GetValueAt(id)

        print("Property {0} not found".format(propName))