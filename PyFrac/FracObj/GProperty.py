from CDistribution import *


class GProperty(object):
    def __init__(self):
        self.m_name_str = ""
        self.m_parentProp_str = ""
        self.m_description_wstr = ""
        self.m_notes_wstr = ""
        self.m_propDistribution = CScalarDistribution()
        self.m_valueType_i32 = 0
        self.m_valueUnits_i32 = 0
        self.m_m_time_f64 = 0.0
        self.m_Minimum_f64 = 0.0
        self.m_Maximum_f64 = 0.0
        self.m_NullVal_f64 = 0.0
        self.m_cacheMean_f64 = 0.0
        self.m_cacheSum_f64 = 0.0
        self.m_cacheDev_f64 = 0.0
        self.m_cacheCount_f64 = 0.0
        self.m_Values = []

    def GetValueAt(self, id):
        if id < len(self.m_Values):
            return self.m_Values[id]
        else:
            print("{0} Value not found ".format(self.m_name_str))

class GPropertyParent(GProperty):
    def __init__(self):
        self.m_obj = object
        super(GPropertyParent, self).__init__()

class GPropertyShadow(GPropertyParent):
    def __init__(self):
        self.m_parentProp_str = ""
        super(GPropertyShadow, self).__init__()

    def Serialize(self, ar):
        self.m_name_str = ar.ReadStr()
        self.m_NullVal_f64 = ar.ReadDbl64()
        self.m_parentProp_str = ar.ReadStr()

class GPropertyShadowTessFracSet(GPropertyShadow):
    def __init__(self):
        self.m_parentProp_str = ""
        super(GPropertyShadowTessFracSet, self).__init__()

class GPropertyIntrinsic(GPropertyParent):
    def __init__(self):
        super(GPropertyIntrinsic, self).__init__()
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            vInt = ar.ReadInt32()

            self.m_Minimum_f64 = ar.ReadDbl64()
            self.m_Maximum_f64 = ar.ReadDbl64()
            self.m_cacheMean_f64 = ar.ReadDbl64()
            self.m_cacheSum_f64 = ar.ReadDbl64()
            self.m_cacheDev_f64 = ar.ReadDbl64()
            if ar.GetObjectVersion() >= 3:
                self.m_cacheCount_f64 = ar.ReadDbl64()

class GFracsetPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GFracsetPropertyIntrinsic, self).__init__()

class GGridPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GGridPropertyIntrinsic, self).__init__()

class GSurfacePropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GSurfacePropertyIntrinsic, self).__init__()

class GBlocksetPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GBlocksetPropertyIntrinsic, self).__init__()

class GTmapPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GTmapPropertyIntrinsic, self).__init__()

class GTessFracPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GTessFracPropertyIntrinsic, self).__init__()

class GWellPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GWellPropertyIntrinsic, self).__init__()

class GLogPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GLogPropertyIntrinsic, self).__init__()

class GPointDataPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GPointDataPropertyIntrinsic, self).__init__()

class GRefsetPropertyIntrinsic(GPropertyIntrinsic):
    def __init__(self):
        super(GRefsetPropertyIntrinsic, self).__init__()

class GPropertyFloat(GProperty):
    def __init__(self):
        super(GPropertyFloat, self).__init__()
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_NullVal_f64 = ar.ReadFl32()

            count = ar.ReadUInt32()
            for i in range(count):
                self.m_Values.append(ar.ReadFl32())

            self.m_propDistribution.Serialize(ar)
            self.m_valueType_i32 = ar.ReadInt32()
            self.m_valueUnits_i32 = ar.ReadInt32()

            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()
            self.m_parentProp_str = ar.ReadStr()

        else:
            pass


class GPropertyDouble(GProperty):
    def __init__(self):
        super(GPropertyDouble, self).__init__()
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_NullVal_f64 = ar.ReadDbl64()

            count = ar.ReadUInt32()
            for i in range(count):
                self.m_Values.append(ar.ReadDbl64())

            self.m_propDistribution.Serialize(ar)
            self.m_valueType_i32 = ar.ReadInt32()
            self.m_valueUnits_i32 = ar.ReadInt32()

            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()
            self.m_parentProp_str = ar.ReadStr()

        else:
            pass


class GPropertyInt(GProperty):
    def __init__(self):
        super(GPropertyInt, self).__init__()

    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_NullVal_f64 = ar.ReadInt32()

            count = ar.ReadUInt32()
            for i in range(count):
                self.m_Values.append(ar.ReadInt32())

            vtInt = ar.ReadInt32()
            vtInt = ar.ReadInt32()



            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()
            self.m_parentProp_str = ar.ReadStr()

        else:
            pass

class GPropertyBit(GProperty):
    def __init__(self):
        super(GPropertyBit, self).__init__()

    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_NullVal_f64 = ar.ReadDbl64()

            nBitsStored = ar.ReadUInt32()
            nElements = ar.ReadUInt32()
            for i in range(nElements):
                self.m_Values.append(ar.ReadUInt32())


            self.m_propDistribution.Serialize(ar)

            vtInt = ar.ReadInt32()
            vtInt = ar.ReadInt32()



            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()
            self.m_parentProp_str = ar.ReadStr()

        else:
            pass

class GPropertyExpression(GProperty):
    def __init__(self):
        super(GPropertyExpression, self).__init__()
        self.m_expression = []
        self.m_propSource_ui32 = 0
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_NullVal_f64 = ar.ReadDbl64()

            self.m_expression = ar.ReadStr()

            bVal = ar.ReadInt32()

            m_static = (0 != bVal)
            if ar.GetArchiveVersion() >= 737:
                bVal = ar.ReadInt32()

            if m_static:
                if ar.GetArchiveVersion() >= 737:

                    count = ar.ReadUInt32()
                    for i in range(count):
                        self.m_Values.append(ar.ReadFl32())


            self.m_propSource_ui32 = ar.ReadUInt32()

            vint = ar.ReadInt32()

            vint = ar.ReadInt32()

            self.m_time_f64 = ar.ReadDbl64()

            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            if ar.GetObjectVersion() >= 7 & m_static:
                count = ar.ReadUInt32()
                for i in range(count):
                    ar.ReadDbl64()

            self.m_parentProp_str = ar.ReadStr()

        else:
           pass

class GPropertyDictionary(GProperty):
    def __init__(self):
        super(GPropertyDictionary, self).__init__()
        self.m_ValueIndexes = []
        self.m_stringTable = []
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()

            count = ar.ReadUInt32()
            for i in range(count):
                self.m_stringTable.append(ar.ReadStr())

            count = ar.ReadUInt32()
            for i in range(count):
                self.m_ValueIndexes.append(ar.ReadInt32())



            #self.m_description_wstr = ar.ReadWStr()
            #self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()
            self.m_parentProp_str = ar.ReadStr()

        else:
            pass

class GPropertyReference(GPropertyParent):
    def __init__(self):
        super(GPropertyReference, self).__init__()
        self.m_propDistribution = CScalarDistribution()
    def Serialize(self, ar):

        if ar.m_isReading:
            self.m_name_str = ar.ReadStr()
            self.m_propDistribution.Serialize(ar)
            self.m_NullVal_f64 = ar.ReadDbl64()
            vtInt = ar.ReadInt32()
            intVal = ar.ReadInt32()

            self.m_description_wstr = ar.ReadWStr()
            self.m_notes_wstr = ar.ReadWStr()

            self.m_time_f64 = ar.ReadDbl64()

            self.m_Minimum_f64 = ar.ReadDbl64()
            self.m_Maximum_f64 = ar.ReadDbl64()

            self.m_parentProp_str = ar.ReadStr()

