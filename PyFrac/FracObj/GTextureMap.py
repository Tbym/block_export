
class GTextureMap(object):
    def __init__(self):
        super(GTextureMap, self).__init__()

    def Serialize(self, ar):
        if ar.m_isReading:
            ar.ReadUint32()
            ar.ReadInt32()
            ar.ReadInt32()
            ar.ReadInt32()
            ar.ReadInt32()
            ar.ReadInt32()
            ar.ReadInt32()

            cp = ar.ReadUInt32()
            for i in range(cp):
                ar.ReadUint8()


