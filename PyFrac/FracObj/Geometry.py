
class Polyhedron(object):
    def __init__(self):
        self.m_Points = []
        self.m_Type = 0
        self.m_Faces = []

class Polygon3DPtr(object):
    def __init__(self):
        self.m_PointIds = []


class Polygon2DPtr(object):
    def __init__(self):
        self.m_Points = []

class Matrix4x4(object):
    def __init__(self):
        self.m_Values = []