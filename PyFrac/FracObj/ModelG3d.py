from G3dScene import *

class ModelG3d(object):
    def __init__(self):
        self.m_scene = G3dScene()

    def Serialize(self, ar):
        nObjects = ar.ReadInt32()
        if nObjects != 1:
            print("Only 1 object expected")
            exit()

        ar.PushArchiveLevel()

        obj = ar.ReadClass()
        self.m_scene.Serialize(ar)
