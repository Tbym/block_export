from G3dCore import *
from CDistribution import *


class CResultDataContainer(G3dCore):
    def __init__(self):
        super(CResultDataContainer, self).__init__()

        self.m_dataTables =[]
        self.m_dataSeries = []

    def Serialize(self,ar):
        super(CResultDataContainer, self).SerializeAttributes(ar)
        self.SerializeV1Tables(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)
        self.SerializeTables(ar)

        m_enableFracPadReport = ar.ReadInt32()


    def SerializeV1Tables(self, ar):

        if ar.m_isReading:
            count = ar.ReadUInt32()
            for i in range(count):
                t = CResultDataTable()
                t.Serialize(ar)
                self.m_dataTables.append(t)

            count = ar.ReadUInt32()
            for i in range(count):
                s = CResultDataSeries()
                s.Serialize(ar)
                self.m_dataSeries.append(t)

    def SerializeTables(self, ar):

        if ar.m_isReading:
            count = ar.ReadUInt32()
            self.m_dataTables = []
            for i in range(count):
                t = CResultDataTable()
                t.SerializeContained(ar)
                self.m_dataTables.append(t)

            count = ar.ReadUInt32()
            self.m_dataSeries = []
            for i in range(count):
                s = CResultDataSeries()
                s.SerializeContained(ar)
                self.m_dataSeries.append(t)



class CResultDataTable(object):
    def __init__(self):

        self.m_dataNames = []
        self.m_dataTypes = []
        self.m_dataValues = []
        self.m_colTitle = ""
        self.m_tableName = ""
        self.m_tableCreatedVersion = ""
        self.m_tableCreatedDate = ""

    def SerializeContained(self, ar):
        ar.PushArchiveLevel()
        if ar.m_isReading:
            o = ar.ReadClass()
            if isinstance(o,CResultDataTable):
                self.Serialize(ar)
        ar.PopArchiveLevel()

    def Serialize(self,ar):

        if ar.m_isReading:

            tableVersion = ar.ReadUInt32()
            count = ar.ReadUInt32()

            for i in range(count):
                self.m_dataNames.append(ar.ReadStr())
                self.m_dataTypes.append(ar.ReadInt32())


            self.m_colTitle = ar.ReadStr()

            count = ar.ReadUInt32()

            for i in range(count):
                vals = ar.ReadUInt32()
                data = []
                for j in range(vals):
                    s = ar.ReadStr()
                    data.append(s)
                self.m_dataValues.append(data)


            self.m_tableName = ar.ReadWStr()
            self.m_tableCreatedVersion = ar.ReadWStr()
            self.m_tableCreatedDate = ar.ReadWStr()

            if ar.GetObjectVersion() >= 5:
                count = ar.ReadUInt32()
                #read section attributes - not used
                for i in range(count):
                    s1 = ar.ReadStr()
                    s2 = ar.ReadStr()



class SObjRef(object):
    def __init__(self):

        self.m_objInx_uint32 = 0
        self.m_elemInx_uint32 = 0
        self.m_elemType_uint32 = 0
        self.m_spareInx_uint32 = 0

class SItemTag(object):
    def __init__(self):
        self.m_tagX_fl64 = 0.0
        self.m_tagY_fl64 = 0.0
        self.m_tagName_wstr = ""
        self.m_tagData_wstr = ""

class CResultDataSeries(object):
    def __init__(self):

        self.m_dataNames = []
        self.m_seriesType_i32 = 0
        self.m_dataNames = []
        self.m_dataX = []
        self.m_dataY = []
        self.m_objectNames = []
        self.m_seriesTitle_wstr = ""
        self.m_xAxisTitle_wstr = ""
        self.m_yAxisTitle_wstr = ""
        self.m_logXAxis_i32 = 0
        self.m_logYAxis_i32 = 0

        self.m_localFrame = CVectorDistLocalFrame()
        self.m_vectorDist = CVectorDistribution()

        self.m_objRefs = []
        self.m_templateName_wstr = ""


        self.m_itemTags = []

        self.m_tableCreatedVersion = ""
        self.m_tableCreatedDate = ""


    def SerializeContained(self, ar):
        ar.PushArchiveLevel()
        if ar.m_isReading:
            o = ar.ReadClass()
            if isinstance(o,CResultDataTable):
                self.Serialize(ar)
        ar.PopArchiveLevel()

    def Serialize(self,ar):

        if ar.m_isReading:

            seriesVersion = ar.ReadUint32()
            self.m_seriesType_i32 = ar.ReadInt32()

            self.m_seriesTitle_wstr = ar.ReadWStr()

            self.m_xAxisTitle_wstr = ar.ReadWStr()
            self.m_logXAxis_i32 = ar.ReadInt32()
            self.m_yAxisTitle_wstr = ar.ReadWStr()
            self.m_logYAxis_i32 = ar.ReadInt32()

            count = ar.ReadUint32()

            for i in range(count):
                self.m_dataNames.append(ar.ReadWStr())
                dataX = []
                dataY = []
                c = ar.ReadUInt32()
                for j in range(c):
                    dataX.append(ar.ReadDbl64())

                c = ar.ReadUint32()
                for j in range(c):
                    dataY.append(ar.ReadDbl64())

                self.m_dataX.append(dataX)
                self.m_dataY.append(dataY)

            if seriesVersion >=1:
                count = ar.ReadUInt32()
                for i in range(count):
                    self.m_dataNames.append(ar.ReadStr())

                count = ar.ReadUint32()
                if count:
                    for i in range(count):
                        c = ar.ReadUInt32()
                        data = []
                        if c:
                            for j in range(c):
                                objRef = SObjRef()
                                objRef.m_objInx_uint32 = ar.ReadUInt32()
                                objRef.m_elemInx_uint32 = ar.ReadUInt32()
                                objRef.m_elemType_uint32 = ar.ReadUInt32()
                                objRef.m_spareInx_uint32_uint32 = ar.ReadUInt32()
                                data.append(objRef)
                        self.m_objRefs.append(data)


                self.m_localFrame.Serialize(ar)
                self.m_vectorDist.Serialize(ar)

                self.m_templateName_wstr = ar.ReadWStr()

                count = ar.ReadUInt32()
                if count:
                    for i in range(count):
                        tag = SItemTag()
                        tag.m_tagX_fl64 = ar.ReadDbl64()
                        tag.m_tagY_fl64 = ar.ReadDbl64()
                        tag.m_tagName_wstr = ar.ReadWStr()
                        tag.m_tagData_wstr = ar.ReadWStr()
                        self.m_itemTags.append(tag)

                additional = ar.ReadUInt32()
                if additional:
                    ar.ReadSkip(additional)

                if ar.GetObjectVersion() >= 4:
                    self.m_tableCreatedVersion = ar.ReadWStr()
                    self.m_tableCreatedDate = ar.ReadWStr()