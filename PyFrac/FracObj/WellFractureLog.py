from G3dCore import *
from GProperties import *


class WellFractureLog(G3dCore):
    def __init__(self):
        super(WellFractureLog, self).__init__()
        self.m_props = GProperties()
        self.m_displayRadius_f32 = 0.0
        self.m_displayRadiusProp_str = ""
        self.m_visibleProp_str = ""


    def Serialize(self,ar):
        self.SerializeAttributes(ar)
        self.SerializeComponents(ar)
        self.SerializeChildren(ar)

        if ar.m_isReading:
            #legVal = ar.ReadUInt32()
            self.m_visibleProp_str = ar.ReadStr()
        else:
            pass

    def SerializeAttributes(self, ar):
        super(WellFractureLog,self).SerializeAttributes(ar)

        flagsOld = ar.ReadUInt32()

        self.m_props.Serialize(ar)

        bOldWellParent = ar.ReadInt32()

        self.m_displayRadius_f32 = ar.ReadFl32()

        self.m_displayRadiusProp_str = ar.ReadStr()