
from FracObj.FracManFile import *
from FracObj.ExportFunctions import *


def ProcessBlocks(fileName):

    #hack to prevent writing to output
    #sys.stdout = open(os.devnull, "w")

    frdFile = FracManFile()
    frdFile.Open(fileName)

    outFolder = "C:\\1Drive\\OneDrive - Golder Associates\\My_projects\\Fracman\\RockWedge\\From_Ciara\\Test\\"
    errorBlocks = []

    names = frdFile.GetObjectsList((CRockBlock))
    for name in names:
        name = name.split("/")[-1]
        obj = frdFile.GetObjectByName(name)
        if not ExportSurfToTS(outFolder + "{0}.ts".format(name), obj, obj.GetOrigin(), inFeet=True):
            errorBlocks.append(name)

    # hack to prevent writing to output
    #sys.stdout = sys.__stdout__

    print("{0} blocks exported from {1}".format(len(names)-len(errorBlocks), fileName))
    print("Error blocks:")
    print(errorBlocks)




if __name__ == "__main__":
    import sys

    fileName = "C:\\Users\\tbym\\My_Projects\\Fracman\\RockWedge\\slope stability_test_TB_v3.frd"
    fileName = "C:\\1Drive\\OneDrive - Golder Associates\\My_projects\\Programming\\Python\\PyFrac\\test3.frd"
    fileName = "C:\\1Drive\\Golder Associates\\19115887, DDMI 2019 Pit and UG Geotech - Phase 3000 P\\05-Technical Work\\Kinematic\\Test\\A21_kinematic_test2.frd"
    fileName = "C:\\1Drive\\Golder Associates\\19115887, DDMI 2019 Pit and UG Geotech - Phase 3000 P\\05-Technical Work\\Kinematic\\Results2\\Rmin-10.0_P32-1.0_kr-1.7\\rea_v00\\rea_v00.frd"
    fileName = "C:\\FracMan_programming\\DFN_LE\\block_export\\Sector_7\\Design_Slope_Sector_7-test.frd"
    ProcessBlocks(fileName)

