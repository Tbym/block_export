#required python modules
import os
import sys
import hashlib
import random
import glob
import multiprocessing
from datetime import datetime



#from .FracObj.vec3 import *
from .FracMan_Definitions import RockWedgeDef
from .FracManThread import FracManThread
from .FracObj.ExportFunctions import ExportSurfToTS
from .FracObj.FracManFile import FracManFile
from .FracObj.CRockBlock import CRockBlock


def compare_hash():
    # compute hash value of the FracMan executable and compare it to the value save on server
    fracmanExe = FracManThread().fracManExePath
    fracmanHash = hashlib.sha1(open(fracmanExe, "rb").read()).hexdigest()

    compareHashPath = "\\\\golder.gds\\gal\\Vancouver\\ComplexData\\RTKC\\Bingham Canyon\\1400458029_SW IR DFN Study\\SIMULATION_RUNS\\PyFrac\\fracman7.9.hash"
    compareHash = ""
    with open(compareHashPath, 'r') as f:
        compareHash = f.read()

    print("Current hash value: {0}".format(fracmanHash))
    print("Required hash value: {0}".format(compareHash))

    if fracmanHash != compareHash:
        print(
            "You are trying to use a different version of FracMan executable. Please download the required .exe file from here:")
        print("{0}".format(compareHashPath))
        return False
    else:
        print("OK - hash values match")
        return True

def process_STS(fileName, outputDir = ""):

    if len(outputDir) == 0:
        outputDir = os.path.dirname(fileName).replace("01_INPUT", "02_OUTPUT")

    with open(fileName,"r") as f:
        lines = f.readlines()
        numberOfBlocks = int(lines[2].strip().split("\t")[1])
        infoLines = lines[31:numberOfBlocks + 31]
        #export file
        #exportFile = os.path.join(outputDir, os.path.basename(fileName))
        exportFile = os.path.join(outputDir, "stats.sts")
        folder = os.path.dirname(exportFile)
        if not os.path.exists(folder):
            os.makedirs(folder)

        with open(exportFile,"w") as outF:
            outF.writelines(infoLines)


def export_blocks_from_Frd(frdFileName, outputDir = ""):

    # hack to prevent writing to output
    sys.stdout = open(os.devnull, "w")

    frdFile = FracManFile()
    frdFile.Open(frdFileName)

    if len(outputDir) == 0:
        outputDir = os.path.dirname(frdFileName).replace("01_INPUT", "02_OUTPUT")

    if not os.path.exists(outputDir):
        os.makedirs(outputDir)


    errorBlocks = []

    names = frdFile.GetObjectsList((CRockBlock))

    for name in names:
        name = name.split("/")[-1]
        if "Block__" in name: #export only single blocks
            obj = frdFile.GetObjectByName(name)
            if not ExportSurfToTS(os.path.join(outputDir,"{0}.ts").format(name), obj, obj.GetOrigin(), inFeet=True):
                errorBlocks.append(name)

    # hack to prevent writing to output
    sys.stdout = sys.__stdout__

    process_STS(frdFileName.replace(".frd", ".sts"))

    #save info about block export
    with open(os.path.join(outputDir,"blocks_export_info.txt"),"w") as f:
        f.write("{0} blocks exported from {1}\n".format(len(names) - len(errorBlocks), frdFileName))
        f.write("Error blocks:\n")
        f.writelines([str(x)+"\n" for x in errorBlocks])


def run_realization(filename):
    frac = FracManThread()
    reaName = os.path.basename(filename)

    # check if no other process is using this file
    status = get_status(os.path.dirname(filename))
    if status == StatusType.PROCESSING:
        print(
            "{0}: {1} IS BEING PROCESSED BY OTHER THREAD".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))
        return

    print("{0}: START {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))

    set_status(os.path.dirname(filename), StatusType.PROCESSING)

    res = frac.Run(filename, showWindow=False)
    frdFile = filename.replace(".fmf", ".frd")
    if res and os.path.exists(frdFile):
        print("{0}: FRACMAN SIMULATION DONE {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))

        # ExportBlocksFromFrd(frdFile)
        # print("{0}: BLOCKS EXPORTED {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))
        set_status(os.path.dirname(filename), StatusType.PROCESSED)
    else:
        print("{0}: FRACMAN SIMULATION ERROR {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))
        set_status(os.path.dirname(filename), StatusType.ERROR)


def compute_all_realizations(outputDir, folderFilter="", nWorkers=1, ignoreStatus=False):


    fmfFiles = glob.glob(os.path.join(outputDir, "**\\*.fmf"), recursive=True)
    processFiles = []

    if not ignoreStatus:
        for fmf in fmfFiles:
            status = get_status(os.path.dirname(fmf))
            if status == (StatusType.NOT_PROCESSED or StatusType.NO_STATUS):
                if folderFilter in fmf:
                    processFiles.append(fmf)

    if nWorkers == 1:
        for fmf in processFiles:
            run_realization(fmf)
    else:
        with multiprocessing.Pool(nWorkers) as pool:
            res = pool.map(run_realization, processFiles)


def create_realization(*params):
    (trend, angle, rea, outputDir, seed, createMacroFunction) = params[0]

    reaName = "Tr-{0:d}_IRA-{1:d}_rea-{2:04d}".format(trend, angle, rea)

    reafolder = "TREND-{0:d}_IRA-{1:d}\\realization_{2:04d}".format(trend, angle, rea)
    dirPath = os.path.join(outputDir, reafolder)
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    outDirPath = dirPath.replace("01_INPUT", "02_OUTPUT")
    if not os.path.exists(outDirPath):
        os.makedirs(outDirPath)

    macroFile = createMacroFunction(trend, angle, reaName, dirPath, seed)

    # set realization status
    set_status(os.path.dirname(macroFile), StatusType.NOT_PROCESSED)


def prepare_all_realizations(nRealizations, settingsParams, outputDir, reaStartNum=0):
    [slopeTrend, slopeAngle, useSeed, seedFile, createMacroFunction] = settingsParams

    # create directory for realization
    subfolder = "01_FRACMAN\\01_INPUT\\DATE_{0}".format(datetime.now().strftime('%Y-%m-%d'))
    dirPath = os.path.join(outputDir, subfolder)
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    seeds = []
    if useSeed:
        if len(seedFile):  # read seed from file
            with open(seedFile, 'r') as f:
                lines = f.readlines()
                seeds = [int(s) for s in lines]
        else:
            seeds = [random.randint(1, 9999999) for i in range(nRealizations)]

            # temporary fix to set Ciara's seed for the first realization
            seeds[0] = 23102019

            # save seed file
            with open(os.path.join(dirPath, "seeds_{0}.txt".format(datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))),
                      'w') as f:
                f.writelines(["{0}\n".format(s) for s in seeds])
    else:
        seeds = [0] * nRealizations

    # prepare all realizations
    for trend in slopeTrend:
        for angle in slopeAngle:
            for rea in range(nRealizations):
                create_realization((trend, angle, rea + reaStartNum, dirPath, seeds[rea], createMacroFunction))

    # print summary
    print("Export location: {0}".format(dirPath))
    print("IRA angles: {0}".format(", ".join([str(x) for x in slopeAngle])))
    print("Trend angles: {0}".format(", ".join([str(x) for x in slopeTrend])))
    print("Number of realizations: {0}".format(nRealizations))
    print("Total number of realizations: {0}".format(nRealizations * len(slopeAngle) * len(slopeTrend)))


class StatusType():
    NOT_PROCESSED = "0"
    PROCESSED = "1"
    PROCESSING = "-1"
    ERROR = "-2"
    NO_STATUS = "-999"


def set_status(dirPath, status):
    """
    Create a file in the directory and write the status information.
    Status: 0 - not calculated realization
            1 - calculated realization
            -1 - calculation running calculation
            -2 - error during calculation
    :param dirPath: directory to store the status
    :param status: status
    :return:
    """
    with open(os.path.join(dirPath, "status"), "w") as f:
        f.writelines(status)


def get_status(dirPath):
    """
    read a status information from the directory
    Status: 0 - not calculated realization
            1 - calculated realization
            -1 - calculation running calculation
            -2 - error during calculation
    :param dirPath: directory to store the status
    :return: status
    """

    statusPath = os.path.join(dirPath, "status")
    if not os.path.exists(statusPath):
        return StatusType.NO_STATUS

    with open(statusPath, "r") as f:
        lines = f.readlines()
        return lines[0]  # return the first line




#Specific function for Bingham sector analyses
def GetRockWedgeDef(name, fracSetNames, surfaceName):

    rwDef = RockWedgeDef()

    rwDef.name = name

    rwDef.sampleName = surfaceName

    for n in fracSetNames:
        rwDef.fracSetNames.append(n)

    #default values
    rwDef.RockDensity = 2700
    rwDef.FrictionAngle = 25
    rwDef.Cohesion = 0
    rwDef.PorePressure = 0

    #run only simple blocks
    rwDef.CombineBlock = 0
    rwDef.SaveBoth = 1
    rwDef.SaveCompositeBlocks = 0
    rwDef.AdjustFOSUsingCompositeBlocks = 1


    rwDef.MaxConnectionLevel = 4

    rwDef.UseStress = False


    return rwDef