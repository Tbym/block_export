import os
import sys
import random

sys.path.append("..\\") #relative path to folder with PyFrac



from PyFrac.FracObj.vec3 import *
from PyFrac.FracMan_Macros import *
from PyFrac.FracManThread import *
from PyFrac.FracObj.ExportFunctions import *
from PyFrac.FracObj.FracManFile import *

from datetime import datetime


def GetTruncatedP32(p32, kr,  r0, rMin, rMax):

    return p32 * (rMin**(2-kr)-rMax**(2-kr))/(r0**(2-kr))

def CreateSetsDefs(rMin, rMax, kr, P32multiplier, regionName):

    ###
    #should be defined based on sector data
    ########

    #all sets are defined - P32 is calculated based on the Rmin
    #default values for all sets
    r0 = 0.25
    kr = kr + 1
    subH_P32 = 1 * P32multiplier
    SW_P32 = 1 * P32multiplier
    SE_P32 = 0.6 * P32multiplier



    sets = []
    # define 3 sets
    name = "SubH_set"
    setSubH = FractureSetDef()

    setSubH.name = name
    setSubH.regionName = regionName
    setSubH.intensityType = IntensityType.P32
    setSubH.intensityValue = GetTruncatedP32(subH_P32, kr, r0, rMin, rMax)
    setSubH.trend = 141.7
    setSubH.plunge = 79.8
    setSubH.fisherK = 9.7
    setSubH.powerLaw_Xmin = r0
    setSubH.powerLaw_D = kr

    setSubH.powerLaw_minVal = rMin
    setSubH.powerLaw_maxVal = rMax
    setSubH.num_sides = 10

    name = "SW_set"
    setSW = FractureSetDef()
    setSW.name = name
    setSW.regionName = regionName
    setSW.intensityType = IntensityType.P32
    setSW.intensityValue = GetTruncatedP32(SW_P32, kr, r0, rMin, rMax)
    setSW.trend = 129
    setSW.plunge = 3
    setSW.fisherK = 13.5
    setSW.powerLaw_Xmin = r0
    setSW.powerLaw_D = kr
    setSW.powerLaw_minVal = rMin
    setSW.powerLaw_maxVal = rMax
    setSW.num_sides = 10

    name = "SE_set"
    setSE = FractureSetDef()
    setSE.name = name
    setSE.regionName = regionName
    setSE.intensityType = IntensityType.P32
    setSE.intensityValue = GetTruncatedP32(SE_P32, kr, r0, rMin, rMax)
    setSE.trend = 43
    setSE.plunge = 5.5
    setSE.fisherK = 5.5
    setSE.powerLaw_Xmin = r0
    setSE.powerLaw_D = kr
    setSE.powerLaw_minVal = rMin
    setSE.powerLaw_maxVal = rMax
    setSE.num_sides = 10

    return [setSubH, setSW, setSE]

def GetRockWedgeDef(name, fracSetNames, surfaceName):


    rwDef = RockWedgeDef()

    rwDef.name = name

    rwDef.sampleName = surfaceName

    for n in fracSetNames:
        rwDef.fracSetNames.append(n)

    #default values
    rwDef.RockDensity = 2700
    rwDef.FrictionAngle = 25
    rwDef.Cohesion = 0
    rwDef.PorePressure = 0

    #run only simple blocks
    rwDef.CombineBlock = 0
    rwDef.SaveBoth = 0
    rwDef.SaveCompositeBlocks = 0
    rwDef.AdjustFOSUsingCompositeBlocks = 0


    rwDef.MaxConnectionLevel = 4

    rwDef.UseStress = False


    return rwDef


def CreateMacro(trend, angle, reaName, outputDir, seed = 0):
    import random

    #create new macro
    macro = FracManMacro()

    #set project to feet
    macro.SetLengthUnits("ft")

    #set seed
    if seed == 0:
        macro.SetSeed(random.randint(1,9999999))
    else:
        macro.SetSeed(seed)

    #create region
    origin = vec3(-3700, -4550, 6200.0)
    boxSize = vec3(1000,1000,500)
    regionName = "region_box"
    macro.InsertRegionBox(regionName, origin, boxSize)


    #define and generate sets
    sets = CreateSetsDefs(30, 1000, 1.9, 1.5, regionName)
    for s in sets:
        macro.AddFractureSet(s)

    macro.GenerateFractureSets(sets)

    #get set names
    setNames = [s.name+"_1" for s in sets]

    #insert bench slope
    slope = SlopeDef()
    slope.name = "bench_slope"
    slope.origin = vec3(-4468.0, -4876.0, 6500.0)
    slope.crestDirTr = trend
    slope.crestDirPl = 0.0
    slope.crestLength = 700
    slope.crestWidth = 50
    slope.toeLength = 700
    slope.toeWidth = 50
    slope.benchAngle = angle
    slope.benchWidth = 50
    slope.benchCount = 12
    slope.benchHeight = 49
    slope.rampWidth = 20

    macro.InsertSlope(slope)



    #rock wedge
    rwDef = GetRockWedgeDef("WedgeAnalysis", setNames, slope.name)

    rwDef.OutputDirectoryName = ""
    #run rockwedge
    macro.RockWedge(rwDef)
    #export results
    macro.ExportObject(rwDef.name+"_stats_1", "ANALYSIS_RESULTS", os.path.join(outputDir, reaName + ".sts"))


    #save the file
    macro.SaveFracFile(os.path.join(outputDir, reaName + ".frd"))

    macro.ExitFrac()
    #save macro
    macroFile = os.path.join(outputDir, reaName + ".fmf")

    macro.WriteMacro(macroFile)

    return macroFile


def ExportBlocksFromFrd(frdFileName, outputDir = ""):

    # hack to prevent writing to output
    sys.stdout = open(os.devnull, "w")

    frdFile = FracManFile()
    frdFile.Open(frdFileName)

    if len(outputDir) == 0:
        outputDir = os.path.join(os.path.dirname(frdFileName), "Blocks")
        outputDir = os.path.dirname(frdFileName).replace("01_FRD_Files", "02_Output")

    if not os.path.exists(outputDir):
        os.makedirs(outputDir)


    errorBlocks = []

    names = frdFile.GetObjectsList((CRockBlock))

    for name in names:
        name = name.split("/")[-1]
        if "Block__" in name: #export only single blocks
            obj = frdFile.GetObjectByName(name)
            if not ExportSurfToTS(os.path.join(outputDir,"{0}.ts").format(name), obj, obj.GetOrigin(), inFeet=True):
                errorBlocks.append(name)

    # hack to prevent writing to output
    sys.stdout = sys.__stdout__

    #save info about block export
    with open(os.path.join(outputDir,"blocks_export_info.txt"),"w") as f:
        f.write("{0} blocks exported from {1}\n".format(len(names) - len(errorBlocks), frdFileName))
        f.write("Error blocks:\n")
        f.writelines([str(x)+"\n" for x in errorBlocks])


def RunSimulationThread(*params):

    (trend, angle, rea, outputDir, seed) = params[0]

    datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # create directory for realization
    dirPath = os.path.join(outputDir, "TREND-{0:d}_IRA-{1:d}\\realization_{2:04d}".format(trend, angle, rea))
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    reaName = "Tr-{0:d}_IRA-{1:d}_rea-{2:04d}".format(trend, angle, rea)

    frac = FracManThread()
    macroFile = CreateMacro(trend, angle, reaName, dirPath, seed)
    print("{0}: START {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))
    frac.Run(macroFile, showWindow=False)
    print("{0}: FRACMAN SIMULATION DONE {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))

    fracManfile = os.path.join(dirPath, reaName + ".frd")
    ExportBlocksFromFrd(fracManfile)
    print("{0}: BLOCKS EXPORTED {1}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), reaName))



def RunAllRealizations(nRealizations, outputDir, settingsParams, nWorkers=1):

    outputDir = os.path.abspath(outputDir)

    [rampTrend, rampAngle, useSeed] = settingsParams

    seeds = []
    if useSeed:
        seeds = [random.randint(1,9999999) for i in range(nRealizations)]
    else:
        seeds = [0] * nRealizations

    realizationSettings = []
    #prepare all simulation cases
    for trend in rampTrend:
        for angle in rampAngle:
            for rea in range(nRealizations):
                realizationSettings.append((trend, angle, rea, outputDir, seeds[rea]))



    #single thread
    if nWorkers == 1:
        for settings in realizationSettings:
            RunSimulationThread(settings)

    else:
        import multiprocessing
        with multiprocessing.Pool(nWorkers) as pool:
            res = pool.map(RunSimulationThread, realizationSettings)



if __name__ == "__main__":



    outputDir = ".\\01_FRACMAN\\01_FRD_Files"


    rampTrend = list(range(293, 313 + 1, 5))
    rampAngle = list(range(33, 41 + 1, 2))

    useSeed = True  #if true we use same seed for all trend and angle variations; ie. fracture geometry differs only per realization

    settingParams = [rampTrend, rampAngle, useSeed]

    numberOfRealizations = 1
    nThreads = 4

    RunAllRealizations(numberOfRealizations, outputDir, settingParams, nThreads)


    i=0






